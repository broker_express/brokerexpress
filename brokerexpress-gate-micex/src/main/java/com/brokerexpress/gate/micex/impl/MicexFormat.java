package com.brokerexpress.gate.micex.impl;

import com.brokerexpress.gate.micex.MicexException;
import com.brokerexpress.gate.micex.model.enums.MicexDataType;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 21.12.10
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */
public class MicexFormat {
   // private List<DecimalFormat> decimalFormatters = new ArrayList<DecimalFormat>();
    private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
    private SimpleDateFormat timeFormatter = new SimpleDateFormat("hhmmss");
    private SimpleDateFormat dateTimeFormatter = new SimpleDateFormat("yyyyMMdd hhmmss");
    private final Charset charset = Charset.forName("Cp1251");
    private final Calendar calendar = Calendar.getInstance();
    private int defYear = 1970;
    private int defMonth = 1;
    private int defDay = 1;
    private int defHour = 0;
    private int defMinute = 0;
    private int defSecond = 0;
    private int defMillisecond = 0;
    private double[] DEC_FACTORS;


    public MicexFormat() {
        DEC_FACTORS = new double[100];
        for (int i = 0; i < DEC_FACTORS.length; i++)
            DEC_FACTORS[i] = (int) Math.pow(10, i);
    }

    private String addSymbolsBefore(String str, int totalLen, char symbol) {
        while (str.length() < totalLen)
            str = symbol + str;
        return str;
    }

    private String addSymbolsAfter(String str, int totalLen, char symbol) {
        while (str.length() < totalLen)
            str = str + symbol;
        return str;
    }

    /*private DecimalFormat getDecimalFormatter(int totalLen, int decimals) {
        while (decimalFormatters.size() <= decimals) {
            char[] formatArr = new char[decimals + 2];
            formatArr[0] = '#';
            formatArr[1] = '.';
            decimalFormatters.add(new DecimalFormat(new String(formatArr)));
        }
        return decimalFormatters.get(decimals);
    } */

    public String formatString(String value, int len) {
        String res = addSymbolsAfter(value.trim(), len, ' ');
        return res;
    }

    public String formatDouble(Double val, int totalLen, int decimals) {
        return formatBigDecimal(BigDecimal.valueOf(val).setScale(decimals, RoundingMode.FLOOR), totalLen);
    }

    /*public static String formatDouble(Double val, int totalLen, String secCode){
         try{
             Integer decimals = securities.getDecimals(secCode);
             return formatDouble(val, totalLen, decimals);
         }
         catch (Exception e){
             return "";
         }
     } */

    public String formatInt(Integer val, int totalLen) {
        return addSymbolsBefore(val.toString(), totalLen, '0');
    }

    public String formatLong(Long val, int totalLen) {
        return addSymbolsBefore(val.toString(), totalLen, '0');
    }

    public String formatDate(Date val) {
        return dateFormatter.format(val);
    }

    public String formatTime(Date val) {
        return timeFormatter.format(val);
    }

    public String formatDateTime(Date date) {
        return dateTimeFormatter.format(date);
    }

    public String formatBigDecimal(BigDecimal value, int totalLen) {
        DecimalFormat format = new DecimalFormat();     //getDecimalFormatter(decimals);

        DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        format.setDecimalFormatSymbols(symbols);

        format.setMaximumFractionDigits(value.scale());
        format.setMinimumFractionDigits(value.scale());

        format.setMaximumIntegerDigits(totalLen - value.scale());
        format.setMinimumIntegerDigits(totalLen - value.scale());
        format.setGroupingUsed(false);

        String str = format.format(value);
        str = str.replace(".", "");
        return str;
    }

    public Object parse(byte[] bytes, int offset, int len, MicexDataType type) throws MicexException {
        switch (type) {
            case TDATE:
                return parseDate(bytes, offset, len);
            case TLONG:
                return parseLong(bytes, offset, len);
            case TCHAR:
                return parseString(bytes, offset, len);
            case TINTEGER:
                return parseInt(bytes, offset, len);
            case TTIME:
                return parseTime(bytes, offset, len);
            default:
                throw new MicexException("Unknown datatype " + type + ".");
        }
    }

    public Integer parseInt(byte[] bytes, int offset, int len) throws MicexException {
        int res = 0;
        int sign = checkSign(bytes, offset);
        if (bytes[offset + len - 1] < (byte) '0')
            return null;
        try {
            int factor = 1;
            for (int i = offset + len - 1; i >= offset; i--) {
                res += (bytes[i] - (byte) '0') * factor;
                factor *= 10;
            }
        } catch (Exception e) {
            throw new MicexException("Int parse error. " + e.getMessage());
        }
        return res * sign;
    }

    public Long parseLong(byte[] bytes, int offset, int len) throws MicexException {
        long res = 0l;
        int sign = checkSign(bytes, offset);
        if (bytes[offset + len - 1] < (byte) '0')
            return null;
        try {
            long factor = 1;
            for (int i = offset + len - 1; i >= offset; i--) {
                res += (long) (bytes[i] - (byte) '0') * factor;
                factor *= 10;
            }
        } catch (Exception e) {
            throw new MicexException("Long parse error. " + e.getMessage());
        }
        return res * sign;
    }

    public Double parseDouble(byte[] bytes, int offset, int len, int decimals) throws MicexException {
        if (decimals < 0)
            throw new MicexException("decimals < 0!!!");

        double res = 0.0;
        int sign = checkSign(bytes, offset);
        if (bytes[offset + len - 1] < (byte) '0')
            return null;
        try {
            double decFactor = DEC_FACTORS[decimals]; //(int)Math.pow(10, decimals);
            int factor = 1;
            for (int i = offset + len - 1; i >= offset; i--) {
                if (offset + len - i <= decimals) {
                    res += (bytes[i] - (byte) '0') / decFactor;
                    decFactor /= 10;
                } else {
                    res += (bytes[i] - (byte) '0') * factor;
                    factor *= 10;
                }
            }
        } catch (Exception e) {
            throw new MicexException("Double parse error. " + e.getMessage());
        }
        return res * sign;
    }

    public String parseString(byte[] bytes, int offset, int len) {
        int endIndex = len + offset - 1;
        for (; offset < len; offset++)
            if (bytes[offset] != ' ')
                break;
        for (; endIndex >= offset; endIndex--)
            if (bytes[endIndex] != ' ')
                break;
        if (offset == endIndex + 1)
            return "";

        //return charset.decode(ByteBuffer.wrap(bytes, offset, endIndex - offset + 1)).toString();
        return new String(bytes, offset, endIndex - offset + 1, charset);
    }

    public Date parseDate(byte[] bytes, int offset, int len) throws MicexException {
        Integer date;
        try {
            date = parseInt(bytes, offset, len);
            if ((date == null) || (date == 0))
                return null;
            int year = date / 10000;
            int month = date / 100 - year * 100;
            int day = date - year * 10000 - month * 100;
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.HOUR_OF_DAY, defHour);
            calendar.set(Calendar.MINUTE, defMinute);
            calendar.set(Calendar.SECOND, defSecond);
            calendar.set(Calendar.MILLISECOND, defMillisecond);
            return calendar.getTime();
        } catch (Exception ex) {
            throw new MicexException("Date parse error. " + ex.getMessage());
        }
    }

    public Date parseDate(int date) throws MicexException {
        try {
            int day = date / 1000000;
            int month = date / 10000 - day * 100;
            int year = date - day * 1000000 - month * 10000;
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month - 1);
            calendar.set(Calendar.DAY_OF_MONTH, day);
            calendar.set(Calendar.HOUR_OF_DAY, defHour);
            calendar.set(Calendar.MINUTE, defMinute);
            calendar.set(Calendar.SECOND, defSecond);
            calendar.set(Calendar.MILLISECOND, defMillisecond);
            return calendar.getTime();
        } catch (Exception ex) {
            throw new MicexException("Date parse error. " + ex.getMessage());
        }
    }

    public Date parseTime(byte[] bytes, int offset, int len) throws MicexException {
        Integer time;
        try {
            time = parseInt(bytes, offset, len);
        } catch (Exception ex) {
            throw new MicexException("Time parse error. " + ex.getMessage());
        }
        if ((time == null) || (time == 0))
            return null;
        return parseTime(time);
    }

    public Date parseTime(int time) throws MicexException {
        try {
            int hour = time / 10000;
            int minute = time / 100 - hour * 100;
            int second = time - hour * 10000 - minute * 100;
            calendar.set(Calendar.YEAR, defYear);
            calendar.set(Calendar.MONTH, defMonth - 1);
            calendar.set(Calendar.DAY_OF_MONTH, defDay);
            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.SECOND, second);
            calendar.set(Calendar.MILLISECOND, defMillisecond);
        } catch (Exception e) {
            throw new MicexException("Time parse error. " + e.getMessage());
        }
        return calendar.getTime();
    }

    private int checkSign(byte[] bytes, int offset) {
        int sign = 1;
        if (bytes[offset] == '-') {
            sign = -1;
            bytes[offset] = '0';
        }
        return sign;
    }
}
