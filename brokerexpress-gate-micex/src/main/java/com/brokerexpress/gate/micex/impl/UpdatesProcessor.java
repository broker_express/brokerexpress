package com.brokerexpress.gate.micex.impl;

import com.brokerexpress.gate.micex.MicexException;
import com.brokerexpress.gate.micex.model.Field;
import com.brokerexpress.gate.micex.model.Table;
import com.brokerexpress.gate.micex.model.enums.FieldFlag;
import com.brokerexpress.gate.micex.model.enums.MicexDataType;
import com.brokerexpress.gate.micex.model.enums.TableFlag;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: brokerexpress
 * Date: 07.06.12
 * Time: 15:50
 * To change this template use File | Settings | File Templates.
 */
public class UpdatesProcessor {
    private List<byte[]> cachedByteArrays = new ArrayList<byte[]>();
    private MicexFormat micexFormat = new MicexFormat();
    private Map<String, Object[]> modifiedRows = new HashMap<String, Object[]>();
    private StringBuilder stringBuilder = new StringBuilder();

    public UpdatesProcessor() {
        //кэшируем массивы длинной до 100 полей
        for (byte i = 1; i <= 255; i++) {
            byte[] arr = new byte[i];
            for (byte j = 0; j < i; j++)
                arr[j] = j;
            cachedByteArrays.add(arr);
        }
    }

    //получаем MAP<Код_Бумаги, Количество дробных знаков> при открытии таблицы
    public Map<String, Integer> getDecimalsMap(Table table, MicexReader micexReader) throws MicexException {
        Map<String, Integer> decimalsMap = new HashMap<String, Integer>();
        //получаем количество строк
        int rowsQty = micexReader.readInt();
        //перебираем последовательно все строки
        for (int rowInd = 0; rowInd < rowsQty; rowInd++) {
            //получаем коллекцию полей для обновления
            byte fieldsQty = micexReader.readByte();
            int rowLen = micexReader.readInt();
            //читаем поля для обновления
            byte[] updatedFields = micexReader.readBytes(fieldsQty);
            if (updatedFields == null)
                updatedFields = cachedByteArrays.get(table.getOutputFields().size());
            //читаем значения для обновления
            byte[] updatedValues = micexReader.readBytes(rowLen);
            int offset = 0;
            int decimals = -1;
            String secCode = "";
            String secBoard = "";
            for (byte fldInd : updatedFields) {
                Field fld = table.getOutputFields().get(fldInd);
                if (fld.getName().equals("DECIMALS"))
                    decimals = micexFormat.parseInt(updatedValues, offset, fld.getSize());
                else if (fld.isSecCode())
                    secCode = micexFormat.parseString(updatedValues, offset, fld.getSize());
                else if (fld.isSecBoard())
                    secBoard = micexFormat.parseString(updatedValues, offset, fld.getSize());
                offset += table.getOutputFields().get(fldInd).getSize();
            }
            if ((secCode.length() > 0) && (decimals >= 0) && (secBoard.length() > 0))
                decimalsMap.put(stringBuilder.delete(0, stringBuilder.length()).append(secBoard).append(secCode).toString(), decimals);
        }
        return decimalsMap;
    }


    public Map<String, Object[]> processTableUpdates(MicexProviderImpl.TableParams tableParams, MicexReader micexReader, Map<String, Integer> decimalsMap) throws MicexException {
        modifiedRows.clear();
        //получаем количество строк
        //FieldsExt outputFields = (FieldsExt)tableParams.getTable().getOutputFields();
        int rowsQty = micexReader.readInt();
        if (rowsQty == 0 || !tableParams.getMicexTableParams().isStoreInMem() || tableParams.getTable().getFlag() == TableFlag.CLEAR_ON_UPDATE) {
            tableParams.getRowByKeyMap().clear();
            if (rowsQty == 0)
                return modifiedRows;
        }
        //перебираем последовательно все строки
        for (int rowInd = 0; rowInd < rowsQty; rowInd++) {
            //получаем коллекцию полей для обновления
            byte fieldsQty = micexReader.readByte();
            int rowLen = micexReader.readInt();
            //читаем поля для обновления
            byte[] updatedFields = micexReader.readBytes(fieldsQty);
            if (updatedFields == null)
                updatedFields = cachedByteArrays.get(tableParams.getTable().getOutputFields().size());
            //читаем значения для обновления
            byte[] updatedValues = micexReader.readBytes(rowLen);
            //применяем обновления

            int offset = 0;
            //формируем первичный ключ и находим строку

            String key = makeKey(tableParams.getTable(), updatedFields, updatedValues);
            Object[] row = tableParams.getRowByKeyMap().get(key);
            if (row == null) {
                row = new Object[tableParams.getTable().getOutputFields().size()];
                tableParams.getRowByKeyMap().put(key, row);
            }
            for (byte fldInd : updatedFields) {
                Field fld = tableParams.getTable().getOutputFields().get(fldInd);
                if (fld.getDataType().equals(MicexDataType.TFIXED)) {
                    int decimals = 2;
                    if (fld.containsFlag(FieldFlag.FIXED1))
                        decimals = 1;
                    else if (fld.containsFlag(FieldFlag.FIXED3))
                        decimals = 3;
                    else if (fld.containsFlag(FieldFlag.FIXED4))
                        decimals = 4;
                    row[fldInd] = micexFormat.parseDouble(updatedValues, offset, fld.getSize(), decimals);
                } else if (fld.getDataType().equals(MicexDataType.TFLOAT)) {
                    //находим secCode
                    String secCode = null;
                    String secBoard = null;
                    int secOffset = 0;
                    for (byte secInd : updatedFields) {
                        Field secFld = tableParams.getTable().getOutputFields().get(secInd);
                        if (secFld.isSecCode())
                            secCode = micexFormat.parseString(updatedValues, secOffset, secFld.getSize());
                        else if (secFld.isSecBoard())
                            secBoard = micexFormat.parseString(updatedValues, secOffset, secFld.getSize());
                        secOffset += secFld.getSize();
                    }
                    if ((secCode == null) || (secBoard == null))
                        throw new MicexException("Can't find secCode field in updates for " + tableParams.getTable().getName() + " table.");

                    Integer decimals = decimalsMap.get(stringBuilder.delete(0, stringBuilder.length()).append(secBoard).append(secCode).toString());

                    if (decimals == null)
                        throw new MicexException("Can't find decimals for " + stringBuilder.toString() + ".");

                    row[fldInd] = micexFormat.parseDouble(updatedValues, offset, fld.getSize(), decimals);
                } else
                    row[fldInd] = micexFormat.parse(updatedValues, offset, fld.getSize(), fld.getDataType());
                offset += fld.getSize();
            }
            modifiedRows.put(key, row);
        }
        return modifiedRows;
    }


    private String makeKey(Table table, byte[] updatedFields, byte[] updatedValues) {
        StringBuilder keyBuilder = new StringBuilder(20);// replaced string concat
        int offset = 0;
        //если таблица обнволяемая - склеиваем первичный ключ
        for (byte fldInd : updatedFields) {
            Field fld = table.getOutputFields().get(fldInd);
            if (fld.isKey())
                keyBuilder.append(micexFormat.parseString(updatedValues, offset, fld.getSize()));
            offset += fld.getSize();
        }
        return keyBuilder.toString();
    }


}
