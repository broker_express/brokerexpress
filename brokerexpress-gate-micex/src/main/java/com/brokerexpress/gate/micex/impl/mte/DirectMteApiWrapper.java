package com.brokerexpress.gate.micex.impl.mte;

import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
* Created by IntelliJ IDEA.
* User: brokerexpress
* Date: 3/13/12
* Time: 4:43 PM
*/
public class DirectMteApiWrapper {
	public static native int MTEGetServInfo(int Idx, PointerByReference msg, IntByReference len);
	public static native int MTEConnect(String params, byte[] result);
	public static native int MTEDisconnect(int Idx);
	public static native int MTEStructure(int handle, PointerByReference msg);
	public static native int MTEOpenTable(int handle, String table, String params, boolean completeFlag, PointerByReference msg);
	public static native int MTECloseTable(int handle, int ref);
	public static native int MTEAddTable(int handle, int tableHandle, int id);
	public static native int MTERefresh(int handle, PointerByReference msg);
	public static native int MTEGetSnapshot(Integer idx, PointerByReference snapshot, IntByReference len);
	public static native int MTESetSnapshot(Integer idx, String snapshot, int len, String msg);
	public static native int MTEFreeBuffer(int idx);
	public static native String MTEErrorMsg(int errCode);
	public static native int MTEExecTrans(int id, String transName, String params,  byte[] resultMsg);
}
