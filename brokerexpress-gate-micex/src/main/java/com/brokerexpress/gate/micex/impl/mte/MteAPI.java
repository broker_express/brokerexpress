package com.brokerexpress.gate.micex.impl.mte;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
//import open.commons.ArchUtils;


//TODO:
public interface MteAPI extends Library {
	public static final String DLL_NAME = null;//ArchUtils.is32Bit() ? "mtesrl.x32" : "mtesrl.x64";

	static MteAPI INSTANCE = "true".equals(System.getProperty("open.jmicex.debug")) ?  (MteAPI)Native.loadLibrary(DLL_NAME, MteAPI.class) : new DirectMteApi() ;

    int MTEGetServInfo(int Idx, PointerByReference msg, IntByReference len);
	int MTEConnect(String params, byte[] result);
	int MTEDisconnect(int Idx);
	int MTEStructure(int handle, PointerByReference msg);
	int MTEOpenTable(int handle, String table, String params, boolean completeFlag, PointerByReference msg);
	int MTECloseTable(int handle, int ref);
	int MTEAddTable(int handle, int tableHandle, int id);
	int MTERefresh(int handle, PointerByReference msg);
	int MTEGetSnapshot(Integer idx, PointerByReference snapshot, IntByReference len);
	int MTESetSnapshot(Integer idx, String snapshot, int len, String msg);
	int MTEFreeBuffer(int idx);
	String MTEErrorMsg(int errCode);
	int MTEExecTrans(int id, String transName, String params,  byte[] resultMsg);

}
