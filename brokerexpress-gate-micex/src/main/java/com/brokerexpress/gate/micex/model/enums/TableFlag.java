package com.brokerexpress.gate.micex.model.enums;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 11.02.11
 * Time: 17:18
 * To change this template use File | Settings | File Templates.
 */
public enum TableFlag {
	NONE
	, UPDATEABLE
	, CLEAR_ON_UPDATE
}
