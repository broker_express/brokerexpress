package com.brokerexpress.gate.micex.impl;

import com.brokerexpress.gate.micex.*;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;
import com.brokerexpress.gate.micex.impl.mte.MteAPI;
import com.brokerexpress.gate.micex.impl.mte.MteException;
import com.brokerexpress.gate.micex.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 30.03.11
 * Time: 17:34
 * To change this template use File | Settings | File Templates.
 */

public class MicexProviderImpl implements MicexProvider {
    private static final Logger logger = LoggerFactory.getLogger(MicexProviderImpl.class);
    private static final int RECONNECT_ATTEMPTS = 100;
    private static final long SHORT_RECONNECT_INTERVAL = 1000;
    private static final long LARGE_RECONNECT_INTERVAL = 10000;
    private static final String SECURITIES_TABLE_NAME = "SECURITIES";
    private static final String INDEXES_TABLE_NAME = "INDEXES";
    private Thread providerThread;
    private MicexConnectionParams connectionParams;
    private int connectionHandle;
    private PointerByReference dataPtr = new PointerByReference();
    private IntByReference sizePtr = new IntByReference();
    private List<TableParams> tablesParams = new ArrayList<TableParams>();
    private Map<String, Table> tableByNameMap = new HashMap<String, Table>();
    private Map<String, Transaction> transactionByNameMap = new HashMap<String, Transaction>();
    private Map<String, Enumerable> enumerableByNameMap = new HashMap<String, Enumerable>();
    private Map<Integer, TableParams> tableParamsByRefMap = new HashMap<Integer, TableParams>();
    private MicexReader reader = new MicexReader();
    private MicexFormat micexFormat = new MicexFormat();
    private Object startSync = new Object();
    private Object stopSync = new Object();
    private UpdatesProcessor updatesProcessor = new UpdatesProcessor();
    private Map<String, Integer> decimalsMap = new HashMap<String, Integer>();
    private List<MicexProviderListener> listeners = new ArrayList<MicexProviderListener>();
    private boolean running = false;

    @Override
    public void setListeners(List<MicexProviderListener> listeners) {
        this.listeners = listeners;
    }

    @Override
    public void setConnectionParams(MicexConnectionParams connectionParams) {
        this.connectionParams = connectionParams;
    }

    @Override
    public void setTablesParams(List<MicexTableParams> micexTableParamsList){
        for (MicexTableParams micexTableParams : micexTableParamsList)
            tablesParams.add(new TableParams(micexTableParams));
    }

    @Override
    public void start(){
        start(false);
    }

    @Override
    public void start(boolean syncronous){
        providerThread = new Thread(new GateThread(), "MICEX_GATE_THREAD_" + connectionParams.getName());
        providerThread.setDaemon(true);
        providerThread.start();
        if (syncronous)
            synchronized (startSync){
                try { startSync.wait(); } catch (InterruptedException ignore) {}
            }
        running = true;
        logger.info("Provider started.");
    }

    @Override
    public void stop(){
        stop(false);
    }

    @Override
    public void stop(boolean syncronous){
        if (syncronous)
            synchronized (stopSync){
                try { stopSync.wait(); } catch (InterruptedException ignore) {}
            }
        running = false;
        logger.info("Provider STOPPED.");
    }

    @Override
    public boolean isRunning() {
        return running;
    }

    public void executeTransaction(ExecutableTransaction transaction) throws MicexException {
        byte[] resultMsg = new byte[256];
        String paramsString = transaction.getParamsString();
        if (paramsString == null)
            throw new MicexException("Not all fields are setted");
        int code = MteAPI.INSTANCE.MTEExecTrans(connectionHandle, transaction.getName(), paramsString, resultMsg);
        if (code < 0){
            String message = getErrMsg(code);
            logger.error(message);
            throw new MteException(message);
        }
        logger.info("Transaction " + transaction.getName() + " executed successfully with params " + transaction.getParamsString() + ".");
        logger.info(new String(resultMsg, Charset.forName("CP1251")));
    }

    private void fillStructures() throws MteException {
        tableByNameMap.clear();
        enumerableByNameMap.clear();
        transactionByNameMap.clear();
        tableParamsByRefMap.clear();
        int code = MteAPI.INSTANCE.MTEStructure(connectionHandle, dataPtr);
        if (code < 0){
            String message = getErrMsg(code);
            logger.error(message);
            throw new MteException(message);
        }
        reader.setData(dataPtr);
        reader.forward(4);//skipping data len field
        reader.forward(reader.readInt());//skipping interface name
        reader.forward(reader.readInt());//skipping interface description
        int itemsQty = reader.readInt();
        for (int i = 0; i < itemsQty; i++){
            Enumerable enumerable = MicexHelper.readEnumerable(reader);
            enumerableByNameMap.put(enumerable.getName(), enumerable);
        }
        itemsQty = reader.readInt();
        for (int i = 0; i < itemsQty; i++){
            Table table = MicexHelper.readTable(reader);
            tableByNameMap.put(table.getName(), table);
        }
        itemsQty = reader.readInt();
        for (int i = 0; i < itemsQty; i++){
            Transaction transaction = MicexHelper.readTransaction(reader);
            transactionByNameMap.put(transaction.getName(), transaction);
        }

        for (MicexProviderListener listener : listeners)
            listener.onStructuresUpdated(tableByNameMap, transactionByNameMap, enumerableByNameMap);

        logger.info("Structures updated.");
    }

    private void addTables() throws MteException {
        for (Map.Entry<Integer, TableParams> entry : tableParamsByRefMap.entrySet()){
            if (entry.getValue().getTable().isUpdateable()){
                int code = MteAPI.INSTANCE.MTEAddTable(connectionHandle, entry.getKey(), entry.getKey());
                if (code < 0)
                    throw new MteException(getErrMsg(code));
            }
        }
    }

    private PointerByReference openTable(TableParams tableParams, boolean isDecimals) throws MicexException {
        Table table = tableByNameMap.get(tableParams.getMicexTableParams().getName());
        if (table == null)
            return null;
        tableParams.setTable(table);
        String formattedParams = null;
        if (table.getInputFields().size() != 0){
            formattedParams = "";
            //если параметры не были переданы - форматируем пустую строку
            for (int i = 0; i < table.getInputFields().size(); i++){
                Field fld = table.getInputFields().get(i);
                Object param = ((i >= tableParams.getParams().length) || (tableParams.getParams()[i] == null)) ? "" : tableParams.getParams()[i];
                formattedParams += micexFormat.formatString(param.toString(), fld.getSize());
            }
        }
        logger.info("Opening table " + table.getName() + ". Params = " + formattedParams + ".");
        long mls = System.currentTimeMillis();
        int code = MteAPI.INSTANCE.MTEOpenTable(connectionHandle, table.getName(), formattedParams, tableParams.getMicexTableParams().isComplete(), dataPtr);
        if (code < 0)
            throw new MteException(getErrMsg(code));
        reader.setData(dataPtr);
        reader.forward(4);
        int ref = reader.readInt();
        tableParamsByRefMap.put(ref, tableParams);

        reader.setOffset(0);//сбрасываем отступ

        if (isDecimals)
            decimalsMap.putAll(updatesProcessor.getDecimalsMap(tableParams.getTable(), reader));
        else {
            Map<String, Object[]> rowsMap = updatesProcessor.processTableUpdates(tableParams, reader, decimalsMap);
            for (MicexProviderListener listener : listeners)
                listener.onTableOpened(tableParams.getMicexTableParams().getName(), rowsMap);
        }

        logger.info("Table " + table.getName() + " opened in " + (System.currentTimeMillis() - mls) + " mls.");
        return dataPtr;
    }


    private int updateTables() throws MicexException {
        int code = MteAPI.INSTANCE.MTERefresh(connectionHandle, dataPtr);
        if (code < 0)
            throw new MteException(getErrMsg(code));
        reader.setData(dataPtr);
        int dataLen = reader.readInt();
        int tablesQty = reader.readInt();
        for (int i = 0; i < tablesQty; i++){
            int ref = reader.readInt();
            TableParams tableParams = tableParamsByRefMap.get(ref);
            if (tableParams == null)
                throw new MicexException("Can't find table by reference " + ref);

            Map<String, Object[]> rows = updatesProcessor.processTableUpdates(tableParams, reader, decimalsMap);

            for (MicexProviderListener listener : listeners)
                listener.onTableUpdated(tableParams.getMicexTableParams().getName(), rows);
        }
        return dataLen;
    }

    private void closeTable(TableParams params) throws MteException {
        int code = MteAPI.INSTANCE.MTECloseTable(connectionHandle, params.getRef());
        if (code < 0)
            throw new MteException(getErrMsg(code));
        tableParamsByRefMap.remove(params.getRef());
    }

    //в плохом случае возврашщаем -1
    private int getSessId() throws MteException {
        int code = MteAPI.INSTANCE.MTEGetServInfo(connectionHandle, dataPtr, sizePtr);
        if (code < 0){
            String message = getErrMsg(code);
            logger.error(message);
            throw new MteException(message);
        }
        reader.setData(dataPtr);
        int state = reader.readInt();
        if (state == 1 || state == 2 )
            return reader.readInt();
        return -1;
    }

    private String getErrMsg(int code) throws MteException {
        return MteAPI.INSTANCE.MTEErrorMsg(code);
    }

    private void connect() throws MteException {
        byte bytes[] = new byte[256];
        for (int i = 0; i < RECONNECT_ATTEMPTS * 2; i++){
            connectionHandle = MteAPI.INSTANCE.MTEConnect(connectionParams.getConnectionString(), bytes);
            if (connectionHandle >= 0)
                return;
            long intervalMls = i < RECONNECT_ATTEMPTS ? SHORT_RECONNECT_INTERVAL : LARGE_RECONNECT_INTERVAL;


            String errMsg = getErrMsg(connectionHandle);
            logger.warn("Can't connect to mtesrl in " + (i + 1) + " attempt with " + intervalMls + "mls interval. " + errMsg);
            try {
                Thread.sleep(intervalMls);
            } catch (InterruptedException e){ throw new MteException("Unknown error.", e); }
        }
        throw new MteException("Can't connect to mtesrl");
    }

    public boolean isConnected(){
        int code = MteAPI.INSTANCE.MTEGetServInfo(connectionHandle, dataPtr, sizePtr);
        if (code < 0)
            return false;
        reader.setData(dataPtr);
        int state = reader.readInt();
        return state == 1 || state == 2;
    }

    private class GateThread implements Runnable{
        private AtomicBoolean working = new AtomicBoolean();


        public void stop(){
            working.set(false);
        }

        @Override
        public void run() {
            working.set(true);
            long timeToSleep;
            int sessId = 0;
            while (working.get()){
                try {
                    //Проверяем наличие соединения со шлюзом
                    if (!isConnected())
                        connect();

                    int newSessId = getSessId();
                    //если изменился id сессии - переоткрываем таболицы
                    if (newSessId != sessId){
                        decimalsMap.clear();

                        logger.info("Session id changed from " + sessId + " to " + newSessId + ".");
                        sessId = newSessId;
                        MteAPI.INSTANCE.MTEFreeBuffer(connectionHandle);

                        for (Map.Entry<Integer, TableParams> entry : tableParamsByRefMap.entrySet())
                            closeTable(entry.getValue());

                        fillStructures();

                        if (tableByNameMap.get(INDEXES_TABLE_NAME) != null){
                            TableParams indexesParams = new TableParams(new MicexTableParams(INDEXES_TABLE_NAME, true, true));
                            openTable(indexesParams, true);
                            closeTable(indexesParams);
                        }
                        if (tableByNameMap.get(SECURITIES_TABLE_NAME) != null){
                            TableParams securitiesParams = new TableParams(new MicexTableParams(SECURITIES_TABLE_NAME, true, true));
                            openTable(securitiesParams, true);
                            closeTable(securitiesParams);
                        }
                        for (TableParams tableParams : tablesParams)
                            openTable(tableParams, false);
                    }

                    synchronized (startSync){ startSync.notifyAll();	}

                    long startUpdTime = System.currentTimeMillis();
                    //добавляем таблицы в список обновления
                    addTables();
                    //вызываем обновление таблиц
                    int len = updateTables();
                    if (len != 0) {
                        if (len < 30000){
                            timeToSleep = (int)(1000 - (System.currentTimeMillis() - startUpdTime));
                            timeToSleep = timeToSleep < 0 ? 0 : timeToSleep;
                            try {
                                Thread.sleep(timeToSleep);
                            } catch (InterruptedException e) {
                                logger.error("Micex provider " + connectionParams.getName() + " forcely STOPPED.", e);
                                stop();
                                break;
                            }
                        }
                    }
                } catch (MteException e){ //если свалились изза шлюзовой ошибки - пытаемся реконнектиться
                    logger.error("Exception during provider working.", e);
                    sessId = 0;
                    try {
                        if (!isConnected())
                            connect();
                    } catch (MteException e1) {
                        logger.error("Can't estabilish connection. Provide stopped.", e1);
                        break;
                    }
                } catch (Throwable e){//если ошибка не в шлюзе - выходим
                    logger.error("Micex provider " + connectionParams.getName() + " forcely STOPPED.", e);
                    stop();
                    break;
                }
            }
            synchronized (stopSync){ stopSync.notifyAll();	}
            MteAPI.INSTANCE.MTEDisconnect(connectionHandle);
        }
    }

    static class TableParams{
        private MicexTableParams micexTableParams;
        private Object[] params;
        private Table table;
        private int ref;
        private Map<String, Object[]> rowByKeyMap = new HashMap<String, Object[]>();

        public TableParams(MicexTableParams micexTableParams) {
            this.micexTableParams = micexTableParams;
        }

        public MicexTableParams getMicexTableParams() {
            return micexTableParams;
        }

        public int getRef() {
            return ref;
        }

        public void setRef(int ref) {
            this.ref = ref;
        }

        public Map<String, Object[]> getRowByKeyMap() {
            return rowByKeyMap;
        }

        public Table getTable() {
            return table;
        }

        public void setTable(Table table) {
            this.table = table;
        }

        public Object[] getParams() {
            return params;
        }

        public void setParams(Object[] params) {
            this.params = params;
        }
    }
}
