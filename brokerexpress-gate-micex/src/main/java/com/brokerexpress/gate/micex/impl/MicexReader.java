package com.brokerexpress.gate.micex.impl;

import com.sun.jna.Pointer;
import com.sun.jna.ptr.PointerByReference;

import java.nio.charset.Charset;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 14.12.10
 * Time: 11:43
 * To change this template use File | Settings | File Templates.
 */

public class MicexReader {
	private static final Charset charset = Charset.forName("Cp1251");
	private int offset = 0;
	private Pointer pointer = null;

	public MicexReader(){}

	public void forward(int steps){
		offset += steps;
	}

	public void backward(int steps){
		offset -= steps;
	}

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public void setData(PointerByReference data){
		pointer = data.getValue();
		offset = 0;
	}

	public byte readByte(){
		byte value = pointer.getByte(offset);//data.getValue().getByte(offset);
		offset += 1;
		return value;
	}

	public byte[] readBytes(int size){
		byte[] bytes = null;
		if (size > 0){
			bytes = pointer.getByteArray(offset, size);
			offset += size;
		}
		return bytes;
	}

	public char readChar(){
		char value = pointer.getChar(offset);
		offset += 1;
		return value;
	}

	public int readInt(){
		int value = pointer.getInt(offset);
		offset += 4;
		return value;
	}

	public String readString(){
		int strLen = readInt();
		String str;
		if (strLen > 0)
			str = new String(pointer.getByteArray(offset, strLen), charset);
		else
			return "";
		offset += strLen;
		return str;
	}

	public String readString(int length){
		String str;
		str = new String(pointer.getByteArray(offset, length), charset);
		offset += length;
		return str;
	}
}
