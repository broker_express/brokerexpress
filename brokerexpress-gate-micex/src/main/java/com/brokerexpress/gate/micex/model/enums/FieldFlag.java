package com.brokerexpress.gate.micex.model.enums;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 14.12.10
 * Time: 16:05
 * To change this template use File | Settings | File Templates.
 */
public enum FieldFlag {
	NONE
	, KEY
	, SEC_CODE
	, NOT_NULL
	, VAR_BLOCK
	, FIXED1
	, FIXED3
	, FIXED4
}
