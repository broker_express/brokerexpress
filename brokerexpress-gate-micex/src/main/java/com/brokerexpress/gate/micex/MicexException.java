package com.brokerexpress.gate.micex;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 17.01.11
 * Time: 10:43
 * To change this template use File | Settings | File Templates.
 */

public class MicexException extends Exception{
	public MicexException(String message){
		super(message);
	}

	public MicexException(Throwable cause){
		super(cause);
	}

	public MicexException(String message, Throwable cause){
		super(message, cause);
	}
}
