package com.brokerexpress.gate.micex.impl.mte;

import com.brokerexpress.gate.micex.MicexException;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 01.03.11
 * Time: 16:38
 * To change this template use File | Settings | File Templates.
 */
public class MteException extends MicexException {
	public MteException(String message) {
		super(message);
	}

	public MteException(Throwable cause) {
		super(cause);
	}

	public MteException(String message, Throwable cause) {
		super(message, cause);
	}
}