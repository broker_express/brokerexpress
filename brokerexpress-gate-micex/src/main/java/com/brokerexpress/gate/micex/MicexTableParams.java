package com.brokerexpress.gate.micex;

/**
 * Created with IntelliJ IDEA.
 * User: brokerexpress
 * Date: 07.06.12
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */

public class MicexTableParams {
    private String name;
    private boolean isComplete = false;
    private boolean storeInMem = true;
    private Object[] params;

    public MicexTableParams(String name, boolean complete, boolean storeInMem) {
        this.name = name;
        isComplete = complete;
        this.storeInMem = storeInMem;
    }

    public MicexTableParams() {
    }

    public boolean isStoreInMem() {
        return storeInMem;
    }

    public void setStoreInMem(boolean storeInMem) {
        this.storeInMem = storeInMem;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean isComplete) {
        this.isComplete = isComplete;
    }
}
