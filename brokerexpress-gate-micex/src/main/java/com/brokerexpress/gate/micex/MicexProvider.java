package com.brokerexpress.gate.micex;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 31.03.11
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 */


public interface MicexProvider {
	void start();
	void stop();
	void start(boolean sync);
	void stop(boolean sync);
    boolean isRunning();
    void setListeners(List<MicexProviderListener> listeners);
    void setTablesParams(List<MicexTableParams> tables);
    void setConnectionParams(MicexConnectionParams connection);
}
