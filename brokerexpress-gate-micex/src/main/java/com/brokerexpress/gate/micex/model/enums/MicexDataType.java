package com.brokerexpress.gate.micex.model.enums;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 14.12.10
 * Time: 16:03
 * To change this template use File | Settings | File Templates.
 */
public enum MicexDataType {
	TCHAR
	, TINTEGER
	, TFIXED
	, TFLOAT
	, TDATE
	, TTIME
	, TLONG
}
