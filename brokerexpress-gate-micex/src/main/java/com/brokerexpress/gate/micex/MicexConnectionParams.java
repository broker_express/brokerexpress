package com.brokerexpress.gate.micex;

/**
 * Created with IntelliJ IDEA.
 * User: brokerexpress
 * Date: 07.06.12
 * Time: 18:39
 * To change this template use File | Settings | File Templates.
 */
public class MicexConnectionParams {
    private String name;
    private String host;
    private String port;
    private String userId;
    private String password;
    private String server;
    private String tradeInterface;
    private String service;

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getTradeInterface() {
        return tradeInterface;
    }

    public void setTradeInterface(String tradeInterface) {
        this.tradeInterface = tradeInterface;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public String getConnectionString(){
        return	  "HOST=" + getHost() + (port !=null && port != "" ? ":" + getPort() : "") + "\n"
                + "USERID=" + getUserId() + "\n"
                + "PASSWORD=" + getPassword() + "\n"
                + "SERVER=" + getServer() + "\n"
                + (service!=null && !service.equals("") ? ("SERVICE=" + service + "\n") : "")
                + "INTERFACE=" + getTradeInterface();
    }
}
