package com.brokerexpress.gate.micex.impl.mte;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;
import com.sun.jna.ptr.IntByReference;
import com.sun.jna.ptr.PointerByReference;

/**
* Created by IntelliJ IDEA.
* User: brokerexpress
* Date: 3/13/12
* Time: 4:41 PM
*/
public class DirectMteApi implements MteAPI{
	NativeLibrary libInst;
	{
		loadLibrary();
	}

	public void loadLibrary(){
		String libName = DLL_NAME;

		libInst = NativeLibrary.getInstance(libName);
		Native.register(DirectMteApiWrapper.class, libInst);
	}

	@Override
	public int MTEGetServInfo(int Idx, PointerByReference msg, IntByReference len) {
		return DirectMteApiWrapper.MTEGetServInfo(Idx, msg, len);
	}

	@Override
	public int MTEConnect(String params, byte[] result) {
		return DirectMteApiWrapper.MTEConnect(params, result);
	}

	@Override
	public int MTEDisconnect(int Idx) {
		return DirectMteApiWrapper.MTEDisconnect(Idx);
	}

	@Override
	public int MTEStructure(int handle, PointerByReference msg) {
		return DirectMteApiWrapper.MTEStructure(handle, msg);
	}

	@Override
	public int MTEOpenTable(int handle, String table, String params, boolean completeFlag, PointerByReference msg) {
		return DirectMteApiWrapper.MTEOpenTable(handle, table, params, completeFlag, msg);
	}

	@Override
	public int MTECloseTable(int handle, int ref) {
		return DirectMteApiWrapper.MTECloseTable(handle, ref);
	}

	@Override
	public int MTEAddTable(int handle, int tableHandle, int id) {
		return DirectMteApiWrapper.MTEAddTable(handle, tableHandle, id);
	}

	@Override
	public int MTERefresh(int handle, PointerByReference msg) {
		return DirectMteApiWrapper.MTERefresh(handle, msg);
	}

	@Override
	public int MTEGetSnapshot(Integer idx, PointerByReference snapshot, IntByReference len) {
		return DirectMteApiWrapper.MTEGetSnapshot(idx, snapshot, len);
	}

	@Override
	public int MTESetSnapshot(Integer idx, String snapshot, int len, String msg) {
		return DirectMteApiWrapper.MTESetSnapshot(idx, snapshot, len, msg);
	}

	@Override
	public int MTEFreeBuffer(int idx) {
		return DirectMteApiWrapper.MTEFreeBuffer(idx);
	}

	@Override
	public String MTEErrorMsg(int errCode) {
		return DirectMteApiWrapper.MTEErrorMsg(errCode);
	}

	@Override
	public int MTEExecTrans(int id, String transName, String params, byte[] resultMsg) {
		return DirectMteApiWrapper.MTEExecTrans(id, transName, params, resultMsg);
	}
}
