package com.brokerexpress.gate.micex.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 31.03.11
 * Time: 18:32
 * To change this template use File | Settings | File Templates.
 */
public class Enumerable {
	protected String name;
	protected String description;
	protected int size;
    private Map<String, String> constants = new HashMap<String, String>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public Map<String, String> getConstants() {
        return constants;
    }

    public void setConstants(Map<String, String> constants) {
        this.constants = constants;
    }
}
