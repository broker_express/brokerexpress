package com.brokerexpress.gate.micex.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 31.03.11
 * Time: 18:36
 * To change this template use File | Settings | File Templates.
 */
public class Transaction {
	protected String name;
	protected String description;
	protected List<Field> inputFields = new ArrayList<Field>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Field> getInputFields() {
        return inputFields;
    }

    public void setInputFields(List<Field> inputFields) {
        this.inputFields = inputFields;
    }
}
