package com.brokerexpress.gate.micex.impl;

import com.brokerexpress.gate.micex.model.Enumerable;
import com.brokerexpress.gate.micex.model.Field;
import com.brokerexpress.gate.micex.model.Table;
import com.brokerexpress.gate.micex.model.Transaction;
import com.brokerexpress.gate.micex.model.enums.FieldFlag;
import com.brokerexpress.gate.micex.model.enums.MicexDataType;
import com.brokerexpress.gate.micex.model.enums.TableFlag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: brokerexpress
 * Date: 07.06.12
 * Time: 18:00
 * To change this template use File | Settings | File Templates.
 */

public class MicexHelper {
    public static Enumerable readEnumerable(MicexReader reader){
        Enumerable enumerable = new Enumerable();
        enumerable.setName(reader.readString());
        enumerable.setDescription(reader.readString());
        enumerable.setSize(reader.readInt());
        //читаем просто так строчку (тип пока не нужен)
        reader.readInt();

        int constsQty = reader.readInt();
        Map<String, String> constantMap = new HashMap<String, String>();
        for (int constInd = 0; constInd < constsQty; constInd++){
            String item = reader.readString();
            String key = item.substring(0, item.indexOf('='));
            String value = item.substring(item.indexOf('=') + 1, item.length());
            constantMap.put(key, value);
        }

        enumerable.setConstants(constantMap);
        return enumerable;
    }


    public static Table readTable(MicexReader reader){
        Table table = new Table();
        int offset = 0;
        table.setName(reader.readString());
        table.setDescription(reader.readString());
        table.setFlag(TableFlag.values()[reader.readInt()]);
        int fieldsQty = reader.readInt();
        List<Field> inputFields = new ArrayList<Field>();
        for (int i = 0; i < fieldsQty; i++){
            Field field = readInputField(reader);
            field.setIndex(i);
            //readInputField(field, reader);
            field.setOffset(offset);
            offset += field.getSize();
            inputFields.add(field);
            //((FieldsExt)table.getInputFields()).add(field);
        }
        fieldsQty = reader.readInt();
        offset = 0;
        List<Field> outputFields = new ArrayList<Field>();

        for (int i = 0; i < fieldsQty; i++){
            Field field = readField(reader);//new FieldExt();
            field.setIndex(i);
            //readField(field, reader);
            field.setOffset(offset);
            offset += field.getSize();
            outputFields.add(field);
        }

        table.setInputFields(inputFields);
        table.setOutputFields(outputFields);

        return table;
    }

    public static Transaction readTransaction(MicexReader reader){
        Transaction transaction = new Transaction();
        int offset = 0;
        transaction.setName(reader.readString());
        transaction.setDescription(reader.readString());
        int fieldsQty = reader.readInt();
        List<Field> fields = new ArrayList<Field>();
        for (int i = 0; i < fieldsQty; i++){
            Field field = readInputField(reader);
            field.setIndex(i);
            field.setOffset(offset);
            offset += field.getSize();
            fields.add(field);
        }
        transaction.setInputFields(fields);
        return transaction;
    }

    private static Field readField(MicexReader reader){
        Field field = new Field();
        field.setName(reader.readString());
        field.setSecboard(field.getName().equals("SECBOARD") || field.getName().equals("INDEXBOARD"));
        field.setDescription(reader.readString());
        field.setSize(reader.readInt());
        field.setDataType(MicexDataType.values()[reader.readInt()]);
        field.setFlags(parseFieldFlags(reader.readInt()));
        field.setEnumName(reader.readString());
        return field;
    }

    private static Field readInputField(MicexReader reader){
        Field field = readField(reader);
        field.setDefVal(reader.readString());
        return field;
    }

    private static List<FieldFlag> parseFieldFlags(int code){
        List<FieldFlag> flags = new ArrayList<FieldFlag>();

        if ((code & 0x0) != 0)
            flags.add(FieldFlag.NONE);
        if ((code & 0x01) != 0)
            flags.add(FieldFlag.KEY);
        if ((code & 0x02) != 0)
            flags.add(FieldFlag.SEC_CODE);
        if ((code & 0x04) != 0)
            flags.add(FieldFlag.NOT_NULL);
        if ((code & 0x08) != 0)
            flags.add(FieldFlag.VAR_BLOCK);
        if ((code & 0x10) != 0)
            flags.add(FieldFlag.FIXED1);
        if ((code & 0x20) != 0)
            flags.add(FieldFlag.FIXED3);
        if ((code & 0x30) != 0)
            flags.add(FieldFlag.FIXED4);

        return flags;
    }

}
