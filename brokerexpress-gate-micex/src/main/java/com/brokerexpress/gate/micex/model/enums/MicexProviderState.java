package com.brokerexpress.gate.micex.model.enums;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 18.03.11
 * Time: 10:38
 * To change this template use File | Settings | File Templates.
 */
public enum MicexProviderState {
	SLEEPING
	, WORKING
	, STOPPED
}
