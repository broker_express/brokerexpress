package com.brokerexpress.gate.micex.model;

import com.brokerexpress.gate.micex.model.enums.TableFlag;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 31.03.11
 * Time: 18:18
 * To change this template use File | Settings | File Templates.
 */
public class Table {
	private TableFlag flag;
    private String name;
    private String description;
    private List<Field> inputFields = new ArrayList<Field>();
    private List<Field> outputFields = new ArrayList<Field>();

    public TableFlag getFlag() {
        return flag;
    }

    public void setFlag(TableFlag flag) {
        this.flag = flag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Field> getInputFields() {
        return inputFields;
    }

    public void setInputFields(List<Field> inputFields) {
        this.inputFields = inputFields;
    }

    public List<Field> getOutputFields() {
        return outputFields;
    }

    public void setOutputFields(List<Field> outputFields) {
        this.outputFields = outputFields;
    }
    public boolean isUpdateable(){
        return getFlag() == TableFlag.UPDATEABLE || getFlag() == TableFlag.CLEAR_ON_UPDATE;
    }

}
