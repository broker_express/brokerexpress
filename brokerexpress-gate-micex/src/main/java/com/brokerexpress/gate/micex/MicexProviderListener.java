package com.brokerexpress.gate.micex;

import com.brokerexpress.gate.micex.model.Enumerable;
import com.brokerexpress.gate.micex.model.Table;
import com.brokerexpress.gate.micex.model.Transaction;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 30.03.11
 * Time: 17:59
 * To change this template use File | Settings | File Templates.
 */
public interface MicexProviderListener {
	void onTableOpened(String name, Map<String, Object[]> rows);
	void onTableUpdated(String name, Map<String, Object[]> modifiedRows);
	void onStructuresUpdated(Map<String, Table> tables, Map<String, Transaction> transcations, Map<String, Enumerable> enumerables);
}
