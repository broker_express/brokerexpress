package com.brokerexpress.gate.micex.model;

import com.brokerexpress.gate.micex.model.enums.FieldFlag;
import com.brokerexpress.gate.micex.model.enums.MicexDataType;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 31.03.11
 * Time: 18:27
 * To change this template use File | Settings | File Templates.
 */
public class Field {
	private String name;
	protected String description;
	protected MicexDataType dataType;
	private List<FieldFlag> flags;
	protected String defVal;
	protected String enumName;
	protected int size;
	private boolean isSecCode = false;
	private boolean isSecboard = false;
	private boolean isKey = false;
	private boolean[] flagMarks = new boolean[FieldFlag.values().length];
    private int index;
    private int offset = 0;


    public void setFlags(List<FieldFlag> flags) {
        this.flags = flags;
        for (FieldFlag flag : flags)
            flagMarks[flag.ordinal()]  = true;
    }

    public void setSecboard(boolean secboard) {
        isSecboard = secboard;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setDataType(MicexDataType dataType) {
        this.dataType = dataType;
    }

    public void setDefVal(String defVal) {
        this.defVal = defVal;
    }

    public void setEnumName(String enumName) {
        this.enumName = enumName;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public void setSecCode(boolean secCode) {
        isSecCode = secCode;
    }

    public void setKey(boolean key) {
        isKey = key;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public String getName() {
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public int getSize() {
		return size;
	}

	public MicexDataType getDataType() {
		return dataType;
	}

	public String getDefVal() {
		return defVal;
	}

	public String getEnumName() {
		return enumName;
	}

	public List<FieldFlag> getFlags(){
		return flags;
	}

	/*protected void addFlag(FieldFlag flag){
		flags.add(flag);
		//for (FieldFlag flagItem : flags)
		if (flag.isSecCode())
			isSecCode = true;
		if (flag.isKey())
			isKey = true;
		flagMarks[flag.ordinal()] = true;
		//else
		//	isSecCode = false;
		//FieldFlag.FIXED1.ordinal()
	}  */

	public boolean containsFlag(FieldFlag flag){
		return flagMarks[flag.ordinal()];
	}

	public boolean isKey(){
		return isKey;
	}

	public boolean isSecCode(){
		return isSecCode;
	}

	public boolean isSecBoard(){
		return isSecboard;
	}
}
