package com.brokerexpress.gate.plaza2.stream.impl;

import com.brokerexpress.gate.plaza2.Plaza2Api;
import com.brokerexpress.gate.plaza2.stream.PlazaRecordReader;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

/**
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 3:08 PM
 */
public class PlazaRecordReaderImpl implements PlazaRecordReader{
	private PlazaStreamImpl stream;
    private static final Plaza2Api plazaApi = Plaza2Api.INSTANCE;
	private int tableIndex;

	public PlazaRecordReaderImpl(PlazaStreamImpl stream){
		this.stream = stream;
	}

	@Override
	public int readInt(int fieldIndex) {
		return plazaApi.readInt(stream.getStreamRef(), fieldIndex);
	}

	@Override
	public long readLong(int fileldIndex) {
		return plazaApi.readLong(stream.getStreamRef(), fileldIndex);
	}

	@Override
	public double readDouble(int fileldIndex) {
		return plazaApi.readDouble(stream.getStreamRef(), fileldIndex);
	}

	@Override
	public String readString(int fileldIndex) {
		return plazaApi.readString(stream.getStreamRef(), fileldIndex);
	}

	Calendar calendar = Calendar.getInstance();
	@Override
	public Date readDate(int fileldIndex) {
		long dateLong = readLong(fileldIndex);
		calendar.set(
					(int) (dateLong / 10000000000000L),
					(int) (dateLong / 100000000000L % 100L) - 1,
					(int) (dateLong / 1000000000 % 100L),
					(int) (dateLong / 10000000 % 100L),
					(int) (dateLong / 100000 % 100L),
					(int) (dateLong / 1000 % 100L)
		);

		calendar.set(Calendar.MILLISECOND, (int)(dateLong % 1000L));
		Date date = calendar.getTime();
		//String dateStr = readString(fileldIndex);
		return date;
	}

	@Override
	public boolean readBool(int fieldIndex) {
		return readInt(fieldIndex) != 0;
	}

	@Override
	public short readShort(int fieldIndex) {
		return (short)readInt(fieldIndex);
	}

	@Override
	public BigDecimal readDecimal(int fieldIndex) {
		return plazaApi.readDecimal(stream.getStreamRef(), fieldIndex);
	}

	@Override
	public int getTableIndex() {
		return tableIndex;
	}

	public void setTableIndex(int tableIndex) {
		this.tableIndex = tableIndex;
	}
}
