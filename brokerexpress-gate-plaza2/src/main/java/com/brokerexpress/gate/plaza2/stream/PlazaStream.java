package com.brokerexpress.gate.plaza2.stream;

import com.brokerexpress.gate.plaza2.PlazaException;

/**
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 1:49 PM
 */
public interface PlazaStream {
	void initialize() throws PlazaException;

	void setStreamName(String streamName) throws PlazaException;
	String getStreamName() throws PlazaException;

	void 	start() throws PlazaException;
	void 	start(boolean sync) throws PlazaException;
	void 	stop() throws PlazaException;
	PlazaStreamState getState() throws PlazaException;
	void reinitialize() throws PlazaException;
	public void setTableRevision(String table, long revId) throws PlazaException;
	public void setLifeNum(long lifeNum) throws PlazaException;
    public String getTableName(int index);
	public String getStreamTableSchema(String table) throws PlazaException;

	public void setStreamHandler(StreamEventHandler streamEventHandler);
	public void setTableHandler(String tableName, TableEventHandler tableHandler);

}
