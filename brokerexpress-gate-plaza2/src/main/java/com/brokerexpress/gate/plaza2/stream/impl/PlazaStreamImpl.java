package com.brokerexpress.gate.plaza2.stream.impl;

import com.brokerexpress.gate.plaza2.Plaza2Api;
import com.brokerexpress.gate.plaza2.Plaza2Callbacks;
import com.brokerexpress.gate.plaza2.PlazaConnection;
import com.brokerexpress.gate.plaza2.PlazaException;
import com.sun.jna.Pointer;
import com.brokerexpress.gate.plaza2.stream.*;
import com.brokerexpress.gate.plaza2.impl.PlazaConnectionImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 1:55 PM

 */
public class PlazaStreamImpl implements PlazaStream{
	private static final Logger logger = LoggerFactory.getLogger(PlazaStream.class);

	private Pointer streamRef;
	private PlazaConnectionImpl plazaConnection;
	private String streamName;
	private PlazaStreamMode mode;
	private String schemaFile;
	private String schemaName;
	private boolean active = false;
	private final PlazaRecordReaderImpl recordReader = new PlazaRecordReaderImpl(this);
	private static final Timer timer = new Timer("State checker",true);
    private static final Plaza2Api plazaApi = Plaza2Api.INSTANCE;
    private final ArrayList<String> tables = new ArrayList<String>();
	public final Object onlineWait = new Object();

	private boolean initialized;

	private Map<String, TableEventHandler> tableHandlersMap = new HashMap<String, TableEventHandler>();
	private final List<TableEventHandler> tableHandlers = new ArrayList<TableEventHandler>();
	private StreamEventHandler streamEventHandler = new DefaultStreamEventHandler();


	public PlazaStreamImpl(){

	}

	public PlazaStreamImpl(PlazaConnection connection, String streamName, PlazaStreamMode mode, String schemaFile, String schemaName) throws PlazaException {
		this.plazaConnection = (PlazaConnectionImpl)connection;
		this.streamName = streamName;
		this.mode = mode;
		this.schemaFile = schemaFile;
		this.schemaName = schemaName;

		initialize();
	}

	public void initialize() throws PlazaException {
		if (initialized)
			return;
			//throw new PlazaException("Stream already initialized");

		this.streamRef = plazaApi.createStream(streamName, mode.getId(), schemaFile, schemaName);
		plazaApi.registerStreamStateChangedCallback(streamRef, stateChangedCallback);
		plazaApi.registerStreamLifeNumChangedCallback(streamRef, lifeNumChangedCallback);
		plazaApi.registerStreamDataBeginCallback(streamRef, dataBeginCallback);
		plazaApi.registerStreamDataEndCallback(streamRef, dataEndCallback);
		plazaApi.registerStreamDataInsertedCallback(streamRef, dataInsertedCallback);
		plazaApi.registerStreamDataDeletedCallback(streamRef, dataDeletedCallback);
		plazaApi.registerStreamDataUpdatedCallback(streamRef, dataUpdatedCallback);
		logger.info("Plaza stream ({}) {}-'{}' created", new Object[]{streamRef, getClass().getName(), streamName });
		initialized = true;
	}

	public void reinitialize() throws PlazaException {
		release();
		tables.clear();
		initialize();
	}

	protected void release() throws PlazaException { // delete stream
		initialized = false;
	}

	@Override
	public void start() throws PlazaException {
		start(false);
	}

	@Override
	public void start(boolean sync) throws PlazaException {
		initialize();

        if (getState() == PlazaStreamState.ONLINE){
            logger.info("Unable to open stream ({}) {}-'{}' started", new Object[]{streamRef, getClass().getName(), streamName });
            return;
        }

		plazaApi.openStream(streamRef, plazaConnection.getConnectionRef());
		active = true;
		logger.info("Plaza stream ({}) {}-'{}' started", new Object[]{streamRef, getClass().getName(), streamName });

		if (sync)
			try {
				synchronized (onlineWait){ onlineWait.wait(); }
				logger.info("Plaza stream ({}) {}-'{}' sync notification sent", new Object[]{streamRef, getClass().getName(), streamName });
			} catch (InterruptedException ignore) {}
	}

	@Override
	public void stop() throws PlazaException {
		if (!active)
			return;

        if (getState() == PlazaStreamState.CLOSE
						&& getState() == PlazaStreamState.CLOSE_COMPLETE){
            logger.info("Unable to stop  stream ({}) {}-'{}' cause it's already closed.", new Object[]{streamRef, getClass().getName(), streamName });
            return;
        }
		active = false;
		plazaApi.closeStream(streamRef);
		logger.info("Plaza stream ({}) {}-'{}' stopped", new Object[]{streamRef, getClass().getName(), streamName });
	}

	@Override
	public PlazaStreamState getState() throws PlazaException {
		return PlazaStreamState.get(plazaApi.getStreamState(streamRef));
	}

	Pointer getStreamRef() {
		return streamRef;
	}

	public void setTableRevision(String table, long revId) throws PlazaException {
		initialize();
		plazaApi.setStreamTableRev(streamRef, table, revId);
	}

	public void setLifeNum(long lifeNum) throws PlazaException {
		initialize();
		plazaApi.setStreamLifeNum(streamRef, lifeNum);
	}

    public String getTableName(int index){
        if (tables.size() > index && tables.get(index) != null)
            return tables.get(index);

        try {
            String name = plazaApi.getStreamTableName(streamRef, index);
            for(;tables.size() <= index; tables.add(null));
            tables.set(index, name);
            return name;
        } catch (PlazaException e) {
            logger.error("Unable to find table {}", index);
            return null;
        }
    }

	public String getStreamTableSchema(String table) throws PlazaException {
		return Plaza2Api.INSTANCE.getStreamTableSchema(getStreamRef(), table);
	}


	protected PlazaRecordReader getReader(int tableIndex)  throws PlazaException  {
		recordReader.setTableIndex(tableIndex);
		return recordReader;
	}

	protected void stateChanged(PlazaStreamState state){
		logger.info("Plaza stream ({}) {}-'{}' state changed to {}", new Object[]{streamRef, getClass().getName(), streamName, state});
		if (state == PlazaStreamState.ONLINE || state == PlazaStreamState.CLOSE_COMPLETE){
			synchronized (onlineWait){ onlineWait.notifyAll(); }
		}
		if (state == PlazaStreamState.ERROR ||
				(state == PlazaStreamState.CLOSE && active &&
						(mode == PlazaStreamMode.RT_REMOTE_ONLINE || mode == PlazaStreamMode.RT_COMBINED_DYNAMIC))){
			timer.schedule(
				new TimerTask() {
					@Override
					public void run() {
						try {
							stop();
							start();
						} catch (PlazaException e) {
							logger.error("Error starting plaza", e);
						}
					}
				},
				1000
			);
		}
	}

	public void setStreamHandler(StreamEventHandler streamEventHandler){
		this.streamEventHandler = streamEventHandler;
	}

	public void setTableHandlersMap(Map<String, TableEventHandler> tableHandlersMap) {
		this.tableHandlersMap = tableHandlersMap;
	}

	public void setTableHandler(String tableName, TableEventHandler tableHandler){
		this.tableHandlersMap.put(tableName,tableHandler);
	}

	private TableEventHandler getTableHandler(int tableIndex) {
		for(;tableHandlers.size() <= tableIndex; tableHandlers.add(null));

		TableEventHandler handler = tableHandlers.get(tableIndex);
		if (handler == null){
			String tableName = getTableName(tableIndex);
			handler = emptyTableHandler;
			for(Map.Entry<String, TableEventHandler> handlerEntry : tableHandlersMap.entrySet())
				if (handlerEntry.getKey().equals(tableName)){
					handler = handlerEntry.getValue();
					break;
				}
			tableHandlers.set(tableIndex, handler);
		}
		return handler;
	}

	private final TableEventHandler emptyTableHandler = new DefaultTableEventHandler();

	//callbacks
	private final Plaza2Callbacks.StreamStateChangedCallback stateChangedCallback = new Plaza2Callbacks.StreamStateChangedCallback() {
		@Override
		public void callback(Pointer stream, int state) {
			try{
				stateChanged(PlazaStreamState.get(state));
				streamEventHandler.stateChanged(PlazaStreamImpl.this, PlazaStreamState.get(state));
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamDataBeginCallback dataBeginCallback = new Plaza2Callbacks.StreamDataBeginCallback() {
		@Override
		public void callback(Pointer stream) {
			try{
				streamEventHandler.dataBegin(PlazaStreamImpl.this);
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamDataEndCallback dataEndCallback = new Plaza2Callbacks.StreamDataEndCallback() {
		@Override
		public void callback(Pointer stream) {
			try{
				streamEventHandler.dataEnd(PlazaStreamImpl.this);
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamDataInsertedCallback dataInsertedCallback = new Plaza2Callbacks.StreamDataInsertedCallback() {
		@Override
		public void callback(Pointer stream, int tableIndex, Pointer record) {
			try{
				getTableHandler(tableIndex).dataInsert(PlazaStreamImpl.this, getReader(tableIndex));
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamDataDeletedCallback dataDeletedCallback = new Plaza2Callbacks.StreamDataDeletedCallback() {
		@Override
		public void callback(Pointer stream, int tableIndex, long recordId, Pointer record) {
			try{
				getTableHandler(tableIndex).dataDelete(PlazaStreamImpl.this, getReader(tableIndex), recordId);
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamDataUpdatedCallback dataUpdatedCallback = new Plaza2Callbacks.StreamDataUpdatedCallback() {
		@Override
		public void callback(Pointer stream, int tableIndex, long recordId, Pointer record) {
			try{
				getTableHandler(tableIndex).dataUpdate(PlazaStreamImpl.this, getReader(tableIndex), recordId);
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};

	private final Plaza2Callbacks.StreamLifeNumChangedCallback lifeNumChangedCallback = new Plaza2Callbacks.StreamLifeNumChangedCallback() {
		@Override
		public void callback(Pointer stream,  int lifeNum) {
            logger.info("Stream {} lifenum changed to {}", PlazaStreamImpl.this.getStreamName(), lifeNum);
			try{
				streamEventHandler.lifeNumChanged(PlazaStreamImpl.this, lifeNum);
			}catch(Exception e){
				logger.error("State changed callback error", e);
			}
		}
	};


	public PlazaConnectionImpl getPlazaConnection() {
		return plazaConnection;
	}

	public void setPlazaConnection(PlazaConnectionImpl plazaConnection) throws PlazaException {
		if (initialized)
			throw new PlazaException("Unable to perform action, stream already initialized");
		this.plazaConnection = plazaConnection;
	}

	public String getStreamName() {
		return streamName;
	}

	public void setStreamName(String streamName) throws PlazaException {
		if (initialized)
			throw new PlazaException("Unable to perform action, stream already initialized");
		this.streamName = streamName;
	}

	public String getSchemaFile() {
		return schemaFile;
	}

	public void setSchemaFile(String schemaFile) throws PlazaException {
		if (initialized)
			throw new PlazaException("Unable to perform action, stream already initialized");
		this.schemaFile = schemaFile;
	}

	public String getSchemaName() {
		return schemaName;
	}

	public void setSchemaName(String schemaName) throws PlazaException {
		if (initialized)
			throw new PlazaException("Unable to perform action, stream already initialized");
		this.schemaName = schemaName;
	}

	public PlazaStreamMode getMode() {
		return mode;
	}

	public void setMode(PlazaStreamMode mode) throws PlazaException {
		if (initialized)
			throw new PlazaException("Unable to perform action, stream already initialized");
		this.mode = mode;
	}

	public boolean isActive() {
		return active;
	}
}
