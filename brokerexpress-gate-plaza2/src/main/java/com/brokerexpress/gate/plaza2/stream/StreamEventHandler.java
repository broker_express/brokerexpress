package com.brokerexpress.gate.plaza2.stream;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 3/31/11
 * Time: 5:48 PM
 */
public interface StreamEventHandler {
	void dataBegin(PlazaStream stream);
	void dataEnd(PlazaStream stream);
	void lifeNumChanged(PlazaStream stream, int lifeNum);
	void stateChanged(PlazaStream stream, PlazaStreamState state);
}
