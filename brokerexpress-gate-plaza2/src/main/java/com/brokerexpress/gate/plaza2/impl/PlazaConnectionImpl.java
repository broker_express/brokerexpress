package com.brokerexpress.gate.plaza2.impl;

import com.brokerexpress.gate.plaza2.Plaza2Api;
import com.brokerexpress.gate.plaza2.PlazaConnection;
import com.brokerexpress.gate.plaza2.PlazaConnectionState;
import com.brokerexpress.gate.plaza2.PlazaException;
import com.sun.jna.Pointer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 12:31 PM
 */
public class PlazaConnectionImpl implements PlazaConnection {
    private static final Logger logger = LoggerFactory.getLogger(PlazaConnection.class);

    private Pointer connectionRef;
	//private int connectionId = 0;
    private String host;
    private int port;
    private String appName;
    private String password;

    private Thread connectionThread;
    private boolean active = false;
    private boolean stop = false;
    private final Object activeWait = new Object();
    private static final Plaza2Api plazaApi = Plaza2Api.INSTANCE;
    private int timeout = 15;
	private int interval = 0;
	private boolean isManualProcessMessages;

	public PlazaConnectionImpl(){

	}

    public PlazaConnectionImpl(String host, int port, String appName, String password) throws PlazaException {
        this(host, port, appName, password, 100);
    }

    public PlazaConnectionImpl(String host, int port, String appName, String password, int timeout) throws PlazaException {
        this.host = host;
        this.port = port;
        this.appName = appName;
        this.password = password;
        this.timeout = timeout;
		initialize();
    }

	private boolean initialized = false;
	public void initialize() throws PlazaException {
		if (initialized)
			throw new PlazaException("Plaza connection is already initialized");

		initialized = true;
		connectionRef = plazaApi.createConnection(host, port, appName, password, 1);
        //if (connectionId == -1)
        //    throw new PlazaException("Unable to create plaza connection");
	}

	@Override
	public void reinitialize() throws PlazaException  {
		release();
		initialize();
	}

	public void release() throws PlazaException  {
		initialized = false;
	}

	@Override
    public void open() throws PlazaException {
		if (!initialized)
			initialize();
		stop = false;
        //Plaza2DirectApi.openConnection(connectionId);
        logger.info("Plaza connection opened ({})", connectionRef.toString());

		connectionThread = new Thread(new ConnectionThread(), "Plaza thread");
		connectionThread.setDaemon(true);
        connectionThread.start();

		try {	Thread.sleep(100 + timeout); } catch (InterruptedException ignore) {}

		/*try {
					synchronized (activeWait) {
						activeWait.wait();
					}
				} catch (InterruptedException e) {
				}*/
    }

    @Override
    public void close() {
		if (!initialized)
			return;
        stop = true;
		if (connectionThread != Thread.currentThread() && connectionThread.isAlive()){
			try {
				synchronized (activeWait) { activeWait.wait(); }
			} catch (InterruptedException e) {}
		}
        logger.info("Plaza connection closed ({})", connectionRef.toString());
    }

	public void processMessages() throws PlazaException {
		plazaApi.processMessages(connectionRef);
	}

    @Override
    public boolean hasState(PlazaConnectionState stateFlag) throws PlazaException {
        return PlazaConnectionState.hasState(plazaApi.getConnectionState(connectionRef), stateFlag);
    }

    public Pointer getConnectionRef() {
        return connectionRef;
    }

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public void setInterval(int interval) {
		this.interval = interval;
	}

	public void setManualProcessMessages(boolean manualProcessMessages) {
		isManualProcessMessages = manualProcessMessages;
	}



	private class ConnectionThread implements Runnable {
        @Override
        public void run() {
            logger.info("Plaza connection thread started ({})", connectionRef.toString());
            /*while(true)
                try {
                    checkConnection();
                    if (hasState(PlazaConnectionState.CS_CONNECTION_CONNECTED))
                        break;
                } catch (PlazaException e) {
                    try { Thread.sleep(1000); } catch (InterruptedException ex) {}
                }
            */
            //int count = 0;
            while (!stop) {
                try {
					processMessages();
					if (interval > 0){
						try {
							Thread.sleep(interval);
						} catch (InterruptedException e) {
						}
					}
					active = true;
                   // if (!active){
                        //active = true;
                        //synchronized (activeWait) { activeWait.notifyAll(); }
                    //}

                    /*if (!hasState(PlazaConnectionState.CS_CONNECTION_CONNECTED))
                        checkConnection();
                    else {
                        if (!active) {
                            synchronized (activeWait) {
                                activeWait.notifyAll();
                            }
                            active = true;
                        }

                        try {
                            plazaApi.processMessages(connectionId, 100);
                        } catch (PlazaException e) {
                            logger.error("Unable to process messages", e);
                        }
                    }*/
                } catch (PlazaException e) {
                    active = false;
                    logger.warn("Unexpected plaza error", e);
                    try { Thread.sleep(1000); } catch (InterruptedException ex) {}
                    //break;
                }
                //count++;
                /*if (count > 100){
                    System.gc();
                    count = 0;
                    try { Thread.sleep(50); } catch (InterruptedException e) { e.printStackTrace(); }
                }*/
            }

            try {
                plazaApi.closeConnection(connectionRef);
				 active = false;
            } catch (PlazaException e) {
                logger.error("Unable to close connection", e);
            }
            synchronized (activeWait) {
                activeWait.notifyAll();
            }
        }

        private int attempts = 0;

        private void checkConnection() throws PlazaException {
            active = false;
            //while(!hasState(PlazaConnectionState.CS_CONNECTION_CONNECTED)){
            try {
                Thread.sleep(attempts < 20 ? 100 : 1000);
            } catch (InterruptedException e) {
            }
            logger.warn("connection opened");
            //Plaza2DirectApi.closeConnection(connectionId);
            try {
                plazaApi.openConnection(connectionRef);
            } catch (PlazaException e) {
                logger.error("Unable to open connection", e);
            }
            attempts++;

            if (hasState(PlazaConnectionState.CS_CONNECTION_CONNECTED)) {
                logger.warn("reconnected");
                attempts = 0;
            }

            //}
        }
    }


}
