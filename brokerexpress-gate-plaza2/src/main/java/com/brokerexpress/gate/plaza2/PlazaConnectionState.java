package com.brokerexpress.gate.plaza2;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 2:53 PM
 * To change this template use File | Settings | File Templates.
 */
public enum PlazaConnectionState {
	 CS_CONNECTION_DISCONNECTED(1),
    CS_CONNECTION_CONNECTED(2),
    CS_CONNECTION_INVALID(4),
    CS_CONNECTION_BUSY(8),
    CS_ROUTER_DISCONNECTED(65536),
    CS_ROUTER_RECONNECTING(131072),
    CS_ROUTER_CONNECTED(262144),
    CS_ROUTER_LOGINFAILED(524288),
    CS_ROUTER_NOCONNECT(1048576);

	private int flag;
	PlazaConnectionState(int flag){
		this.flag = flag;
	}

    public int getFlag(){
        return flag;
    }

	private static final Map<Integer, PlazaConnectionState> lookup = new HashMap<Integer, PlazaConnectionState>();
    static {
        for (PlazaConnectionState state : PlazaConnectionState.values())
            lookup.put(state.flag, state);
    }

	public static boolean hasState(int state, PlazaConnectionState flag){
        return  (state & flag.flag) != 0;
    }
}
