package com.brokerexpress.gate.plaza2;

/**
 * User: brokerexpress
 * Date: 3/1/11
 * Time: 9:08 PM
 */
public interface PlazaMessage {
	//long getMessageRef();
	String getMessageName();
	void setField(String name, int value) throws PlazaException;
	void setField(String name, long value) throws PlazaException ;
	void setField(String name, String value) throws PlazaException ;
	void setField(String name, double value) throws PlazaException ;
	void send(long timeout) throws PlazaException ;
	void sendAsync(long timeout) throws PlazaException ;
	void sendAsync(long timeout, Plaza2Callbacks.SendMessageAsyncCallback callback) throws PlazaException ;


	String 	readString(String name) throws PlazaException ;
	int 	readInt(String fieldName) throws PlazaException ;
	long 	readLong(String fieldName) throws PlazaException ;
	double readDouble(String fieldName) throws PlazaException ;

	public void release() throws PlazaException;

}
