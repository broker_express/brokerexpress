package com.brokerexpress.gate.plaza2;

import com.brokerexpress.gate.plaza2.impl.PlazaConnectionImpl;
import com.brokerexpress.gate.plaza2.stream.impl.PlazaStreamImpl;
import org.springframework.context.support.GenericXmlApplicationContext;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 4/23/12
 * Time: 2:20 PM
 */
public class X {
	PlazaConnectionImpl plazaConnection;
	PlazaStreamImpl stream;
	PlazaStreamImpl stream2;

	public static void main(String[] args) throws PlazaException {
		GenericXmlApplicationContext context = new GenericXmlApplicationContext();
		context.getEnvironment().setActiveProfiles("combined");
		context.load(
				"classpath:/open/plaza2n/stream-open.xml"
		);
		context.refresh();
		final PlazaConnectionImpl plazaConnection = context.getBean(PlazaConnectionImpl.class);
		final PlazaStreamImpl stream = context.getBean("stream", PlazaStreamImpl.class);
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
					plazaConnection.initialize();
					plazaConnection.open();
					stream.setTableRevision("fut_vcb", 0);
					stream.start(true);
					//stream2.start(true);
					plazaConnection.close();
				} catch (PlazaException e) {
					e.printStackTrace();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}});
		thread.start();

	}
	public void x(){
		//final PlazaConnectionImpl plazaConnection = context.getBean(PlazaConnectionImpl.class);
		//final PlazaStreamImpl stream = context.getBean("stream", PlazaStreamImpl.class);
		//final PlazaStreamImpl stream2 = context.getBean("stream2", PlazaStreamImpl.class);


	}

	public void setPlazaConnection(PlazaConnectionImpl plazaConnection) {
		this.plazaConnection = plazaConnection;
	}

	public void setStream(PlazaStreamImpl stream) {
		this.stream = stream;
	}

	public void setStream2(PlazaStreamImpl stream2) {
		this.stream2 = stream2;
	}
}
