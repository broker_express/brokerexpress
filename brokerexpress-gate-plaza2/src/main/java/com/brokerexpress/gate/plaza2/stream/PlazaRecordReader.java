package com.brokerexpress.gate.plaza2.stream;

import java.math.BigDecimal;
import java.util.Date;

/**
  * User: brokerexpress
 * Date: 2/16/11
 * Time: 3:04 PM
  */
public interface PlazaRecordReader {
	int 		readInt		(int fieldIndex);
	long 		readLong	(int fieldIndex);
	double 	readDouble	(int fieldIndex);
	String 	readString	(int fieldIndex);
	Date 		readDate	(int fieldIndex);

	boolean 	readBool	(int fieldIndex);
	short 		readShort	(int fieldIndex);

	BigDecimal readDecimal	(int fieldIndex);

	int 		getTableIndex();

	//Object 	readObject	(int fieldIndex);
}
