
package com.brokerexpress.gate.plaza2.stream;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 3/31/11
 * Time: 5:49 PM
 */
public class DefaultStreamEventHandler implements StreamEventHandler{
	@Override
	public void dataBegin(PlazaStream stream) {}

	@Override
	public void dataEnd(PlazaStream stream) {}

	@Override
	public void lifeNumChanged(PlazaStream stream, int lifeNum) {}

	@Override
	public void stateChanged(PlazaStream stream, PlazaStreamState state) {}
}
