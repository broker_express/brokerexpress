package com.brokerexpress.gate.plaza2.stream;

/**
* Created by IntelliJ IDEA.
* User: brokerexpress
* Date: 2/16/11
* Time: 1:54 PM
* To change this template use File | Settings | File Templates.
*/
public enum PlazaStreamMode {
	RT_LOCAL(0),
	RT_COMBINED_SNAPSHOT(1),
	RT_COMBINED_DYNAMIC(2),
	RT_REMOTE_SNAPSHOT(3),
	RT_REMOVE_DELETED(4),
	RT_REMOTE_ONLINE(8);

	private int id;

	PlazaStreamMode(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
}
