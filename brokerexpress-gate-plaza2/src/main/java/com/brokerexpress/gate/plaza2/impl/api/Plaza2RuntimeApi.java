package com.brokerexpress.gate.plaza2.impl.api;

import com.sun.jna.Library;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;
import com.sun.jna.ptr.*;
//import open.commons.ArchUtils;
import com.brokerexpress.gate.plaza2.Plaza2Callbacks;

/**
 * User: brokerexpress
 * Date: 2/22/11
 * Time: 3:30 PM
 */

//TODO:zxzdfsdf
public interface Plaza2RuntimeApi extends Library {

    static Plaza2RuntimeApi INSTANCE = null;//(Plaza2RuntimeApi) Native.loadLibrary(ArchUtils.is32Bit() ? "open.plaza2n.x32" : "open.plaza2n.x64", Plaza2RuntimeApi.class);

    String createConnection(String host, int port, String app, String password, int timeout, PointerByReference conRef);

    String openConnection(Pointer id);

    String closeConnection(Pointer id);

    String processMessages(Pointer id);

    String getConnectionState(Pointer id, IntByReference state);

    String createStream(String streamName, int mode, String schemaFile, String schemaName, PointerByReference streamId);

    String openStream(Pointer id, Pointer connectionId);

    String closeStream(Pointer id);

    String setStreamLifeNum(Pointer id, long lifenum);

    String setStreamTableRev(Pointer id, String table, long rev);

    String getStreamState(Pointer id, IntByReference state);

    String getStreamTableSchema(Pointer streamId, String tableName, Memory buf);

    String getStreamTableName(Pointer streamId, int tableIndex, Memory mem);

    // void addStreamTable(int streamId, String tableName, String schema);
    String registerStreamDataInsertedCallback(Pointer id, Plaza2Callbacks.StreamDataInsertedCallback callback);

    String registerStreamDataUpdatedCallback(Pointer id, Plaza2Callbacks.StreamDataUpdatedCallback callback);

    String registerStreamDataDeletedCallback(Pointer id, Plaza2Callbacks.StreamDataDeletedCallback callback);

    String registerStreamDataBeginCallback(Pointer id, Plaza2Callbacks.StreamDataBeginCallback callback);

    String registerStreamDataEndCallback(Pointer id, Plaza2Callbacks.StreamDataEndCallback callback);

    String registerStreamLifeNumChangedCallback(Pointer id, Plaza2Callbacks.StreamLifeNumChangedCallback callback);

    String registerStreamStateChangedCallback(Pointer id, Plaza2Callbacks.StreamStateChangedCallback callback);

    void readRecordInt(Pointer streamId, int fieldIndex, IntByReference value);

    void readRecordLong(Pointer streamId, int fieldIndex, LongByReference value);

    void readRecordDouble(Pointer streamId, int fieldIndex, DoubleByReference value);

	void readRecordDecimal(Pointer streamId, int fieldIndex, LongByReference longRef, ShortByReference shortRef);

    void readRecordWString(Pointer streamId, int fieldIndex, Memory buf);
	
	String createMessageFactory(String schemaFile, PointerByReference factoryId);
	String createMessage(Pointer factoryId, String name, PointerByReference msgId);
	String releaseMessage(Pointer msgId);
	String setMessageField(Pointer msgId, String fieldName, String value);
	String sendMessage(Pointer msgId, Pointer connection, long timeout);
	String sendMessageAsync(Pointer msgId, Pointer connectionId, long timeout, Plaza2Callbacks.SendMessageAsyncCallback callback);
	String registerSendMessageAsyncCallback(Plaza2Callbacks.SendMessageAsyncCallback callback);
	void readMessageInt(Pointer msgId, String fieldName, IntByReference value);
	void readMessageLong(Pointer msgId, String fieldName, LongByReference value);
	void readMessageDouble(Pointer msgId, String fieldName, DoubleByReference value);
	void readMessageWString(Pointer msgId, String fieldName, Memory buf);

    String registerLoggingCallback(Plaza2Callbacks.LoggingCallback callback);


}
