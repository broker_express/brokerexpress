package com.brokerexpress.gate.plaza2.stream.impl;

import com.brokerexpress.gate.plaza2.PlazaException;
import com.brokerexpress.gate.plaza2.stream.TableEventHandler;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 4/11/11
 * Time: 5:30 PM
 */
public class PlazaProviderImpl {
	private PlazaStreamImpl[] streams;

	public void setStreams(PlazaStreamImpl[] streams) {
		this.streams = streams;
	}

	public void setTableHandler(String table, TableEventHandler handler) throws PlazaException {
		for (PlazaStreamImpl stream : streams){
			String[] tableNameParts = table.split(":");
			if (tableNameParts.length != 2)
				throw new PlazaException("Table name is incorrect, should be like 'STREAM_NAME:table_name'.");

			if (tableNameParts[0].equals(stream.getStreamName()))
				stream.setTableHandler(tableNameParts[1], handler);
		}
	}
}
