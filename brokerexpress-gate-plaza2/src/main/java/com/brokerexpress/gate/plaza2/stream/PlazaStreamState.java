package com.brokerexpress.gate.plaza2.stream;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 2:14 PM
  */
public enum PlazaStreamState {
	CLOSE 	(0),
	LOCAL_SNAPSHOT (1),
	REMOTE_SNAPSHOT (2),
	ONLINE	(3),
	CLOSE_COMPLETE (4),
	REOPEN (5),
	ERROR	(6);

	private int id;
	PlazaStreamState(int id){
		this.id = id;
	}

	private static final Map<Integer, PlazaStreamState> lookup = new HashMap<Integer, PlazaStreamState>();
    static {
        for (PlazaStreamState state : PlazaStreamState.values())
            lookup.put(state.id, state);
    }

	public static PlazaStreamState get(int id){
        return lookup.get(id);
    }
}
