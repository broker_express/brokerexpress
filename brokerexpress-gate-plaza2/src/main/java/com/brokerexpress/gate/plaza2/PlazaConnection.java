package com.brokerexpress.gate.plaza2;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 12:29 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PlazaConnection {
	//int getConnectionRef();
	void open() throws PlazaException;
	void close();
	boolean hasState(PlazaConnectionState stateFlag) throws PlazaException;

	public String getHost();

	public void setHost(String host);

	public int getPort();

	public void setPort(int port);

	public String getAppName();

	public void setAppName(String appName);

	public String getPassword();

	public void setPassword(String password);

	public int getTimeout();

	public void setTimeout(int timeout);

	void reinitialize() throws PlazaException;

	void processMessages() throws PlazaException;
}
