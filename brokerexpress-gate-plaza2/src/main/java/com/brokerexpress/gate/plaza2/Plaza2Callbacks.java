package com.brokerexpress.gate.plaza2;

import com.sun.jna.Callback;
import com.sun.jna.Pointer;

/**
  * User: brokerexpress
 * Date: 2/22/11
 * Time: 2:33 PM
  */
public interface Plaza2Callbacks {
    public interface StreamDataInsertedCallback extends Callback {
            void callback(Pointer stream, int tableIndex, Pointer record);
        }
        public interface StreamDataUpdatedCallback extends Callback {
            void callback(Pointer stream, int tableIndex, long recordId, Pointer record);
        }
        public interface StreamDataDeletedCallback extends Callback {
            void callback(Pointer stream, int tableIndex, long recordId, Pointer record);
        }
        public interface StreamDataBeginCallback extends Callback {
            void callback(Pointer stream);
        }
        public interface StreamDataEndCallback extends Callback {
            void callback(Pointer stream);
        }
        public interface StreamLifeNumChangedCallback extends Callback {
            void callback(Pointer stream, int lifenum);
        }
        public interface StreamStateChangedCallback extends Callback {
            void callback(Pointer stream, int state);
        }
		public interface SendMessageAsyncCallback extends Callback {
            void callback(Pointer message, long errorCode);
        }
        public interface LoggingCallback extends Callback {
            void callback(int level, String message);
        }

}
