package com.brokerexpress.gate.plaza2;

import java.util.HashMap;
import java.util.Map;

/**
 * User: brokerexpress
 * Date: 2/2/11
 * Time: 12:59 AM
 */
public enum PlazaSessionState {
    APPOINTED(0),   //  Сессия назначена. Нельзя ставить заявки, но можно удалять.
    ACTIVE(1),      //  Сессия идет. Можно ставить и удалять заявки.
    SUSPENDED(2),   //  Приостановка торгов по всем инструментам. Нельзя ставить заявки, но можно удалять.
    TERMINATED(3),  //  Сессия принудительно завершена. Нельзя ставить и удалять заявки.
    FINISHED(4);     //  Сессия завершена по времени. Нельзя ставить и удалять заявки.

    private final int id;
    private static final Map<Integer, PlazaSessionState> lookup = new HashMap<Integer, PlazaSessionState>();

    static {
        for (PlazaSessionState state : PlazaSessionState.values())
            lookup.put(state.getId(), state);
    }

    PlazaSessionState(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public static PlazaSessionState get(int id){
        return lookup.get(id);
    }
}