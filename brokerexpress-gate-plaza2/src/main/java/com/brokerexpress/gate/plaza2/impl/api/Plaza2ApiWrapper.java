package com.brokerexpress.gate.plaza2.impl.api;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/12
 * Time: 11:31 AM
 */
public class Plaza2ApiWrapper {
	static{
		File file = new File("./P2ClientGate.ini");
		if (!file.exists())
			copyFromResource("/com/brokerexpress/gate/plaza2/P2ClientGate.ini", file);
		file = new File("./P2ClientGate_trace.ini");
		if (!file.exists())
			copyFromResource("/com/brokerexpress/gate/plaza2/P2ClientGate_trace.ini", file);
	}

	private static void copyFromResource(String resourceName, File destFile){
		try{
			InputStream is = Plaza2ApiWrapper.class.getResourceAsStream(resourceName);
			OutputStream os = new FileOutputStream(destFile);
			byte[] buffer = new byte[4096];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
			os.close();
			is.close();
		}catch (Exception ignore){

		}
	}
}
