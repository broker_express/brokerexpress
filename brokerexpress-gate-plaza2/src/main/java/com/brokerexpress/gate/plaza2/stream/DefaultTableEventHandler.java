package com.brokerexpress.gate.plaza2.stream;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 3/31/11
 * Time: 5:35 PM
 */
public class DefaultTableEventHandler implements TableEventHandler {
	@Override
	public void dataInsert(PlazaStream stream, PlazaRecordReader reader) {
	}

	@Override
	public void dataDelete(PlazaStream stream, PlazaRecordReader reader, long recordId) {
	}

	@Override
	public void dataUpdate(PlazaStream stream, PlazaRecordReader reader, long recordId) {
	}
}
