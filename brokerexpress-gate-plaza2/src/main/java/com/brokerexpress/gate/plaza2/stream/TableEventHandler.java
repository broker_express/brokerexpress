package com.brokerexpress.gate.plaza2.stream;

/**
 * User: brokerexpress
 * Date: 3/5/11
 * Time: 3:07 PM
 */
public interface TableEventHandler {
    void dataInsert(PlazaStream stream, PlazaRecordReader reader);
    void dataDelete(PlazaStream stream, PlazaRecordReader reader, long recordId);
    void dataUpdate(PlazaStream stream, PlazaRecordReader reader, long recordId);
}
