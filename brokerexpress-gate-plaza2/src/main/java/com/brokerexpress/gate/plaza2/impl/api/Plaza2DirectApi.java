package com.brokerexpress.gate.plaza2.impl.api;

import com.sun.jna.*;
import com.sun.jna.ptr.*;
//import open.commons.ArchUtils;
import com.brokerexpress.gate.plaza2.Plaza2Api;
import com.brokerexpress.gate.plaza2.Plaza2Callbacks;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
  * User: brokerexpress
 * Date: 2/15/11
 * Time: 8:01 PM
  */
public class Plaza2DirectApi {
	static final Logger logger = LoggerFactory.getLogger(Plaza2DirectApi.class);
	static final Plaza2Callbacks.LoggingCallback loggerCallback = new Plaza2Callbacks.LoggingCallback(){
		public void callback(int streamId, String message){
			logger.info(message);
		}
	};
	static NativeLibrary libInst;
	static {
		loadLibrary();
	}

	public static void loadLibrary(){
		String libName = Plaza2Api.DLL_NAME;
        logger.info("Registering native lib '{}'", libName);
        libInst = NativeLibrary.getInstance(libName);
		Native.register(Plaza2DirectApi.class, libInst);
		//registerLoggingCallback(loggerCallback);
	}

	public static void reloadLibrary(){
		Native.unregister(Plaza2DirectApi.class);
		libInst.dispose();
		loadLibrary();
	}

	public static native String createConnection(String host, int port, String app, String password, int timeout, PointerByReference conId);
	public static native String openConnection(Pointer id);
	public static native String closeConnection(Pointer id);
	public static native String processMessages(Pointer id);
	public static native String getConnectionState(Pointer id, IntByReference state);
	public static native String createStream(String streamName, int mode, String schemaFile, String schemaName, PointerByReference streamId);
	public static native String openStream(Pointer id, Pointer connectionId);
	public static native String closeStream(Pointer id);
	public static native String setStreamLifeNum(Pointer id, long lifenum);
	public static native String setStreamTableRev(Pointer id, String table, long rev);
	public static native String getStreamState(Pointer id, IntByReference state);
	public static native String getStreamTableSchema(Pointer streamId, String tableName, Memory buf);
    public static native String getStreamTableName(Pointer streamId, int tableIndex, Memory buf);
    //public static native void addStreamTable(int streamId, String tableName, String schema);
	public static native String registerStreamDataInsertedCallback(Pointer id, Plaza2Callbacks.StreamDataInsertedCallback callback);
	public static native String registerStreamDataUpdatedCallback(Pointer id, Plaza2Callbacks.StreamDataUpdatedCallback callback);
	public static native String registerStreamDataDeletedCallback(Pointer id, Plaza2Callbacks.StreamDataDeletedCallback callback);
	public static native String registerStreamDataBeginCallback(Pointer id, Plaza2Callbacks.StreamDataBeginCallback callback);
	public static native String registerStreamDataEndCallback(Pointer id, Plaza2Callbacks.StreamDataEndCallback callback);
	public static native String registerStreamLifeNumChangedCallback(Pointer id, Plaza2Callbacks.StreamLifeNumChangedCallback callback);
	public static native String registerStreamStateChangedCallback(Pointer id, Plaza2Callbacks.StreamStateChangedCallback callback);
	public static native String readRecordInt(Pointer stream, int fieldIndex, IntByReference value);
	public static native String readRecordLong(Pointer stream, int fieldIndex, LongByReference value);
	public static native String readRecordDouble(Pointer stream, int fieldIndex, DoubleByReference value);
	public static native String readRecordWString(Pointer stream, int fieldIndex, Memory buf);
	public static native void   readRecordDecimal(Pointer streamId, int fieldIndex, LongByReference longRef, ShortByReference shortRef);

	public static native String createMessageFactory(String schemaFile, PointerByReference factoryId);
	public static native String createMessage(Pointer factoryId, String name,PointerByReference msgId);
	public static native String releaseMessage(Pointer msgId);
	public static native String setMessageField(Pointer msgId, String fieldName, String value);
	public static native String sendMessage(Pointer msgId, Pointer connection, long timeout);
	public static native String sendMessageAsync(Pointer msgId, Pointer connectionId, long timeout, Plaza2Callbacks.SendMessageAsyncCallback callback);
	public static native String registerSendMessageAsyncCallback(Plaza2Callbacks.SendMessageAsyncCallback callback);
	public static native void readMessageInt(Pointer msgId, String fieldName, IntByReference value);
	public static native void readMessageLong(Pointer msgId, String fieldName, LongByReference value);
	public static native void readMessageDouble(Pointer msgId, String fieldName, DoubleByReference value);
	public static native void readMessageWString(Pointer msgId, String fieldName, Memory buf);


	public static native String registerLoggingCallback(Plaza2Callbacks.LoggingCallback callback);


}
