package com.brokerexpress.gate.plaza2.impl;

import com.sun.jna.Pointer;
import com.brokerexpress.gate.plaza2.Plaza2Api;
import com.brokerexpress.gate.plaza2.PlazaException;
import com.brokerexpress.gate.plaza2.PlazaMessage;

/**
  * User: brokerexpress
 * Date: 3/1/11
 * Time: 9:30 PM
  */
public class PlazaMessageFactoryImpl implements PlazaMessageFactory{
	private Pointer factoryRef;
	private String schemaFile;
	private PlazaConnectionImpl plazaConnection;

	public PlazaMessageFactoryImpl(){
	}

	public PlazaMessageFactoryImpl(String schemaFile) throws PlazaException {
		this.schemaFile = schemaFile;
		initialize();
	}

	public void initialize() throws PlazaException {
		factoryRef = Plaza2Api.INSTANCE.createMessageFactory(schemaFile);
	}

	public Pointer getFactoryRef() throws PlazaException  {
		return factoryRef;
	}

	public String getSchemaFile() {
		return schemaFile;
	}

	public void setSchemaFile(String schemaFile) {
		this.schemaFile = schemaFile;
	}

	@Override
	public PlazaMessage createMessage(String messageName) throws PlazaException {
		return createMessage(messageName, plazaConnection);
	}

	public PlazaMessage createMessage(String messageName, PlazaConnectionImpl plazaConnection) throws PlazaException {
		Pointer messageRef = Plaza2Api.INSTANCE.createMessage(getFactoryRef(), messageName);
		PlazaMessageImpl message = new PlazaMessageImpl(messageRef);
		message.setPlazaConnection(plazaConnection);
		return message;
	}



	public void setPlazaConnection(PlazaConnectionImpl plazaConnection) {
		this.plazaConnection = plazaConnection;
	}
}
