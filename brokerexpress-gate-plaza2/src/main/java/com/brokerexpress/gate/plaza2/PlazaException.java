package com.brokerexpress.gate.plaza2;

/**
 * Created by IntelliJ IDEA.
 * User: brokerexpress
 * Date: 2/16/11
 * Time: 1:51 PM
 */
public class PlazaException extends Exception{
	public PlazaException() {
	}

	public PlazaException(String message) {
		super(message);
	}

	public PlazaException(String message, Throwable cause) {
		super(message, cause);
	}

	public PlazaException(Throwable cause) {
		super(cause);
	}
}
