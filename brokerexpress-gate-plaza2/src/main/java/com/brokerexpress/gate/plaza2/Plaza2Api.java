package com.brokerexpress.gate.plaza2;

import com.sun.jna.Pointer;
//import open.commons.ArchUtils;
import com.brokerexpress.gate.plaza2.impl.api.Plaza2DirectApiWrapper;
import com.brokerexpress.gate.plaza2.impl.api.Plaza2RuntimeApiWrapper;

import java.math.BigDecimal;

/**
  * User: brokerexpress
 * Date: 2/22/11
 * Time: 3:38 PM
  */

//TODO:
public interface Plaza2Api {
	public static final String DLL_NAME = null;//ArchUtils.is32Bit() ? "open.plaza2n.x32" : "open.plaza2n.x64";
    public static final Plaza2Api INSTANCE = "true".equals(System.getProperty("open.plaza2n.debug")) ? new Plaza2RuntimeApiWrapper() : new Plaza2DirectApiWrapper();

    public Pointer createConnection(String host, int port, String app, String password, int timeout) throws PlazaException;
	public void openConnection(Pointer id) throws PlazaException;
	public void closeConnection(Pointer id) throws PlazaException;
	public void processMessages(Pointer id) throws PlazaException;
	public int getConnectionState(Pointer id) throws PlazaException;
	public Pointer createStream(String streamName, int mode, String schemaFile, String schemaName) throws PlazaException;
	public void openStream(Pointer id, Pointer connection) throws PlazaException;
	public void closeStream(Pointer id) throws PlazaException;
	public void setStreamLifeNum(Pointer id, long lifenum) throws PlazaException;
	public void setStreamTableRev(Pointer id, String table, long rev) throws PlazaException;
	public int getStreamState(Pointer id) throws PlazaException;
	public String getStreamTableSchema(Pointer streamId, String tableName) throws PlazaException;
    public String getStreamTableName(Pointer streamId, int tableIndex) throws PlazaException;
    //public void addStreamTable(int streamId, String tableName, String schema);
	public void registerStreamDataInsertedCallback(Pointer id, Plaza2Callbacks.StreamDataInsertedCallback callback) throws PlazaException;
	public void registerStreamDataUpdatedCallback(Pointer id, Plaza2Callbacks.StreamDataUpdatedCallback callback) throws PlazaException;
	public void registerStreamDataDeletedCallback(Pointer id, Plaza2Callbacks.StreamDataDeletedCallback callback) throws PlazaException;
	public void registerStreamDataBeginCallback(Pointer id, Plaza2Callbacks.StreamDataBeginCallback callback) throws PlazaException;
	public void registerStreamDataEndCallback(Pointer id, Plaza2Callbacks.StreamDataEndCallback callback) throws PlazaException;
	public void registerStreamLifeNumChangedCallback(Pointer id, Plaza2Callbacks.StreamLifeNumChangedCallback callback) throws PlazaException;
	public void registerStreamStateChangedCallback(Pointer id, Plaza2Callbacks.StreamStateChangedCallback callback) throws PlazaException;
	public int readInt(Pointer streamId, int fieldIndex);
	public long readLong(Pointer streamId, int fieldIndex);
	public double readDouble(Pointer streamId, int fieldIndex);
    public String readString(Pointer streamId, int fieldIndex);
	public BigDecimal readDecimal(Pointer streamId, int fieldIndex);

	public  Pointer createMessageFactory(String schemaFile) throws PlazaException;
	public  Pointer createMessage(Pointer factoryId, String name) throws PlazaException;
	public  void releaseMessage(Pointer msgId) throws PlazaException;
	public  void setMessageField(Pointer msgId, String fieldName, String value) throws PlazaException;
	public  void sendMessage(Pointer msgId, Pointer connection, long timeout) throws PlazaException;
	public  void sendMessageAsync(Pointer msgId, Pointer connection, long timeout, Plaza2Callbacks.SendMessageAsyncCallback callback) throws PlazaException;
	public  void registerSendMessageAsyncCallback(Plaza2Callbacks.SendMessageAsyncCallback callback);
	public int readMessageInt(Pointer msgId, String fieldName);
	public long readMessageLong(Pointer msgId, String fieldName);
	public double readMessageDouble(Pointer msgId, String fieldName);
    public String readMessageString(Pointer msgId, String fieldName);

	public void reloadLibrary();
	//public void getLibraryInstance();


}
