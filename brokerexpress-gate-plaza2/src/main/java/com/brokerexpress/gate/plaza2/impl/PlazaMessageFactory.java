package com.brokerexpress.gate.plaza2.impl;


import com.brokerexpress.gate.plaza2.PlazaException;
import com.brokerexpress.gate.plaza2.PlazaMessage;

/**

 * User: brokerexpress
 * Date: 3/1/11
 * Time: 9:24 PM
 */
public interface PlazaMessageFactory {
	PlazaMessage createMessage(String messageName) throws PlazaException;
}
