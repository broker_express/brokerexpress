package com.brokerexpress.gate.plaza2;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

public class PlazaHelper {
	public static Field[] parseTableSchema(String schema) {
		String[] fieldStrings = schema.split(",");

		ArrayList<Field> fieldList = new ArrayList<Field>();
		for (String fieldString : fieldStrings) {
			String[] fieldParts = fieldString.split("=");
			Class<?> type = toDataType(fieldParts[1]);

			Field field;
			if (!String.class.equals(type))
				field = new Field(fieldParts[0], type);
			else
				field = new Field(fieldParts[0], type, Integer.valueOf(fieldParts[1].substring(1)));

			fieldList.add(field);
		}
		Field[] fields = fieldList.toArray(new Field[fieldList.size()]);
		return fields;
	}

	public static Class<?> toDataType(String fieldType) {
		switch (fieldType.charAt(0)) {
			case 'i':
				return fieldType.charAt(1) > '4' ? Long.class : Integer.class;
			case 'u':
				return fieldType.charAt(1) > '4' ? Long.class : Integer.class;
			case 'd':
				return BigDecimal.class;
			case 'f':
				return BigDecimal.class;
			case 'c':
				return String.class;
			case 't':
				return Date.class;
		}
		return null;
	}

	public static String normalizeName(String fieldName, boolean firstCap) {
		String[] parts = fieldName.split("_");
		StringBuilder builder = new StringBuilder();
		builder.append((firstCap ? Character.toUpperCase(parts[0].charAt(0)) : Character.toLowerCase(parts[0].charAt(0))) + parts[0].substring(1));
		for (int i = 1; i < parts.length; i++)
			builder.append(Character.toUpperCase(parts[i].charAt(0)) + parts[i].substring(1));

		return builder.toString();
	}

	public static class Field {
		private String name;
		private Class<?> type;
		private int length;

		public Field(String name, Class<?> type) {
			this.name = name;
			this.type = type;
		}

		public Field(String name, Class<?> type, int length) {
			this.name = name;
			this.type = type;
			this.length = length;
		}

		public String getName() {
			return name;
		}

		public Class<?> getType() {
			return type;
		}

		public int getLength() {
			return length;
		}
	}

}